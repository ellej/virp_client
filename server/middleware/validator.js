/**
 * A validator (v) wrapper so that custom validators
 * can be used as middleware
 */
module.exports = function(v) {
  return (req, res, next) => {
    const { error } = v(req.body);

    // if invalid req body, pass err to err handler
    if (error)
      return next({
        status: 400,
        message: error.details[0].message
      });

    // otherwise pass req to next middleware
    next();
  };
};
