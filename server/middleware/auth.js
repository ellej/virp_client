require('dotenv').config();
const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(' ')[1];

    jwt.verify(token, process.env.JWT_SECRET_KEY, (err, decoded) => {
      if (err)
        return next({
          status: 401,
          message: 'Invalid token.'
        });

      req.user = decoded;
      next();
    });
  }
  catch (err) {
    next({
      status: 401,
      message: 'No token provided.'
    });
  }
};
