const winston = require('winston');

// handle errors (from within the request processing pipeline)
module.exports = function(err, req, res, next) {
  // log error
  winston.error(err.message, err);

  // send error to client
  res.status(err.status || 500).send({
    error: {
      code: err.status,
      message: err.message || 'Something went wrong...'
    }
  });
};
