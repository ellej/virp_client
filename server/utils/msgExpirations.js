const moment = require('moment');

exports.expirations = {
  ONE_MIN: '1m',
  FIVE_MIN: '5m',
  THIRTY_MIN: '30m',
  ONE_HOUR: '1h',
  SIX_HOURS: '6h',
  TWELVE_HOURS: '12h',
  ONE_DAY: '1d',
  SEVEN_DAYS: '7d',
  THIRTY_DAYS: '30d',
  DEFAULT: '30d'
};

exports.getExpirationDate = (expiresIn) => {
  switch (expiresIn) {
    case this.expirations.ONE_MIN:
      return moment().add(1, 'm').format();
    case this.expirations.FIVE_MIN:
      return moment().add(5, 'm').format();
    case this.expirations.THIRTY_MIN:
      return moment().add(30, 'm').format();
    case this.expirations.ONE_HOUR:
      return moment().add(1, 'h').format();
    case this.expirations.SIX_HOURS:
      return moment().add(6, 'h').format();
    case this.expirations.TWELVE_HOURS:
      return moment().add(12, 'h').format();
    case this.expirations.ONE_DAY:
      return moment().add(1, 'd').format();
    case this.expirations.SEVEN_DAYS:
      return moment().add(7, 'd');
    case this.expirations.THIRTY_DAYS:
      return moment().add(30, 'd').format();
    default:
      return moment().add(30, 'd').format();
  }
};
