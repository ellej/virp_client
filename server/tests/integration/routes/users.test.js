const request = require('supertest');
const mongoose = require('mongoose');
const { User } = require('../../../models/user');

describe('API calls to /api/v1/users', () => {

  let server;
  let username;
  let userId;
  let token;
  const nonExistentId = new mongoose.Types.ObjectId();

  // start the server and create a user before each test
  beforeEach(async () => {
    server = require('../../../app');
    const user = await User.create({
      username: 'john',
      password: 'mySecretPassword'
    });

    username = user.username;
    userId = user._id.toString();
    token = user.generateAuthToken();
  });

  // clean up the test db and close server after each test
  afterEach(async () => {
    await User.deleteMany({});
    await server.close();
  });

  describe('GET /username/:username', () => {

    const getApiEndpoint = () => `/api/v1/users/username/${username}`;

    const exec = () => (
      request(server)
        .get(getApiEndpoint())
        .set('Authorization', `bearer ${token}`)
    );

    it('should return user if username exists', async () => {
      const res = await exec();

      expect(res.status).toBe(200);
      expect(res.body).toHaveProperty('data');
      expect(res.body.data).toHaveProperty('_id', userId);
      expect(res.body.data).toHaveProperty('username', username);
    });

    it('should return user if username exists, case-insensitive', async () => {
      username = username.toUpperCase();

      const res = await exec();

      expect(res.status).toBe(200);
      expect(res.body).toHaveProperty('data');
      expect(res.body.data).toHaveProperty('_id', userId);
      expect(res.body.data).toHaveProperty('username', username.toLowerCase());
    });

    it('should return 404 if there is no such username', async () => {
      username = 'noSuchUsername';

      const res = await exec();

      expect(res.status).toBe(404);
    });

    it('should return 401 if client is not logged in', async () => {
      // simulate a non-logged in user
      token = '';

      const res = await exec();

      expect(res.status).toBe(401);
    });
  });

  describe('GET /id/:id', () => {

    const getApiEndpoint = () => `/api/v1/users/id/${userId}`;

    const exec = () => (
      request(server)
        .get(getApiEndpoint())
        .set('Authorization', `bearer ${token}`)
    );

    it('should return user if user exists', async () => {
      const res = await exec();

      expect(res.status).toBe(200);
      expect(res.body).toHaveProperty('data');
      expect(res.body.data).toHaveProperty('_id', userId);
      expect(res.body.data).toHaveProperty('username', username);
    });

    it('should return 404 if no user with the given ID exists', async () => {
      userId = nonExistentId;

      const res = await exec();

      expect(res.status).toBe(404);
    });

    it('should return 400 if ID is an invalid objectId', async () => {
      userId = 'invalidObjectId';
      
      const res = await exec();

      expect(res.status).toBe(400);
    });

    it('should return 401 if client is not logged in', async () => {
      // simulate a non-logged in user
      token = '';

      const res = await exec();

      expect(res.status).toBe(401);
    });
  });

  describe('PUT /id/:id (crypto data)', () => {

    let pubKey;
    let privKey;
    let salt;

    const getApiEndpoint = () => `/api/v1/users/id/${userId}`;

    const exec = () => (
      request(server)
        .put(getApiEndpoint())
        .set('Authorization', `bearer ${token}`)
        .send({ pubKey, privKey, salt })
    );

    beforeEach(() => {
      pubKey = 'pubKey';
      privKey = 'privKey';
      salt = 'salt';
    });

    it('should update the user in the DB if user exists', async () => {
      await exec();

      const updatedUser = await User.findById(userId);

      expect(updatedUser.pubKey).toBe(pubKey);
      expect(updatedUser.privKey).toBe(privKey);
      expect(updatedUser.salt).toBe(salt);
    });

    it('should return 400 if privKey is less than 1 character', async () => {
      privKey = '';

      const res = await exec();

      expect(res.status).toBe(400);
    });

    it('should return 400 if privKey is more than 3000 characters', async () => {
      privKey = new Array(3002).join('a');

      const res = await exec();

      expect(res.status).toBe(400);
    });

    it('should return 400 if pubKey is less than 1 character', async () => {
      pubKey = '';

      const res = await exec();

      expect(res.status).toBe(400);
    });

    it('should return 400 if pubKey is more than 1024 characters', async () => {
      pubKey = new Array(1026).join('a');

      const res = await exec();

      expect(res.status).toBe(400);
    });

    it('should return 400 if salt is less than 1 character', async () => {
      salt = '';

      const res = await exec();

      expect(res.status).toBe(400);
    });

    it('should return 400 if salt is more than 1024 characters', async () => {
      salt = new Array(1026).join('a');

      const res = await exec();

      expect(res.status).toBe(400);
    });

    it('should return 404 if no user with the given ID exists', async () => {
      userId = nonExistentId;

      const res = await exec();

      expect(res.status).toBe(404);
    });

    it('should return 400 if ID is an invalid objectId', async () => {
      userId = 'invalidObjectId';
      
      const res = await exec();

      expect(res.status).toBe(400);
    });

    it('should return 401 if client is not logged in', async () => {
      // simulate a non-logged in user
      token = '';

      const res = await exec();

      expect(res.status).toBe(401);
    });
  });
});
