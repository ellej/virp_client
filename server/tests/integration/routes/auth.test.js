const request = require('supertest');
const { User, encryptPW } = require('../../../models/user');

describe('API calls to /api/v1/auth', () => {

  let server;

  // start the server before each test
  beforeEach(() => { server = require('../../../app') });

  // clean up the test db and close server after each test
  afterEach(async () => {
    await User.deleteMany({});
    await server.close();
  });

  describe('POST /signup', () => {
    const API_ENDPOINT = '/api/v1/auth/signup';
    const testUser = {};

    // server request
    const exec = () => (
      request(server)
        .post(API_ENDPOINT)
        .send(testUser)
    );

    beforeEach(() => {
      testUser.username = 'john';
      testUser.password = 'mySecretPassword';
    });

    it('should return 400 if username is less than 4 chars', async () => {
      testUser.username = '123';

      const res = await exec();

      expect(res.status).toBe(400);
    });

    it('should return 400 if username is more than 50 chars', async () => {
      testUser.username = new Array(52).join('a');

      const res = await exec();

      expect(res.status).toBe(400);
    });

    it('should return 400 if password is less than 7 chars', async () => {
      testUser.password = '123456';

      const res = await exec();

      expect(res.status).toBe(400);
    });

    it('should return 400 if password is more than 1024 chars', async () => {
      testUser.password = new Array(1026).join('a');
      
      const res = await exec();

      expect(res.status).toBe(400);
    });

    it('should return 400 if username is taken', async () => {
      // create a user beforehand
      await exec();

      // create the same user again
      const res = await exec();

      expect(res.status).toBe(400);
    });

    it('should return 400 if username is taken despite lower or upper case', async () => {
      // create a user beforehand
      await exec(); // this uses 'john'

      // create user with same name but uppercase
      testUser.username = 'JOHN';

      const res = await exec();

      expect(res.status).toBe(400);
    });

    it('should not save user to DB if username is taken', async () => {
      // create a user beforehand
      await exec();

      // create the same user again
      await exec();

      const docCount = await User.countDocuments({ username: testUser.username });

      expect(docCount).toBe(1);
    });

    // happy path tests
    it('should save user to DB if it is valid', async () => {
      await exec();

      const user = await User.find({ username: testUser.username });

      expect(user).not.toBeNull();
    });

    it('should return user if it is valid', async () => {
      const res = await exec();
      const user = res.body.data;

      expect(user).toHaveProperty('_id');
      expect(user).toHaveProperty('username', testUser.username);
      expect(user).toHaveProperty('token');
    });
  });

  describe('POST /login', () => {
    const API_ENDPOINT = '/api/v1/auth/login';
    const loginCredentials = {};

    // server request
    const exec = () => (
      request(server)
        .post(API_ENDPOINT)
        .send(loginCredentials)
    );

    beforeEach(async () => {
      loginCredentials.username = 'john';
      loginCredentials.password = 'mySecretPassword';

      // create a user to log in with
      await User.create({
        username: loginCredentials.username,
        password: await encryptPW(loginCredentials.password)
      });
    });

    it('should return 400 if invalid username', async () => {
      loginCredentials.username = 'invalidUsername';

      const res = await exec();

      expect(res.status).toBe(400);
    });

    it('should return 400 if invalid password', async () => {
      loginCredentials.password = 'invalidPassword';

      const res = await exec();

      expect(res.status).toBe(400);
    });

    // happy path
    it('should return auth token if valid credentials', async () => {
      const res = await exec();

      expect(res.body.data).toHaveProperty('token');
      expect(res.body.data.token).toBeTruthy();
    });
  });
});
