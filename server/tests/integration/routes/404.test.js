const request = require('supertest');

describe('API calls to non existing route', () => {
  const INVALID_API_ENDPOINT = '/api/invalid_api_endpoint';
  let server;

  beforeEach(() => { server = require('../../../app') });

  afterEach(async () => { await server.close(); });

  it('should return 404 if route does not exist', async () => {
    const res = await request(server).post(INVALID_API_ENDPOINT).send({});
    
    expect(res.status).toBe(404);
  });
});
