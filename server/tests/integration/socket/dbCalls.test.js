const mongoose = require('mongoose');
const { User, encryptPW } = require('../../../models/user');
const { Chat } = require('../../../models/chat');
const { Message } = require('../../../models/message');
const { expirations } = require('../../../utils/msgExpirations');
const {
  updateUserSocketId, resetUserSocketId, loadSettings, createChat, loadChats,
  setMsgDestruction, createMsg, loadChatMsgs, getCryptoData, storeCryptoData, loadContacts,
  storeContact, loadSearchRes, blockContact, unblockContact, deleteContact, deleteChat,
  loadUnreadMsgs, markAsRead, updateDefaultMsgDestruction, updateDecryptMsgByDefault,
  updateNewMsgNotification, deleteAccount
} = require('../../../socket/dbCalls');

describe('DB calls triggered by socket events', () => {

  let server;
  let sender;
  let receiver;
  let chat;

  const nonExistentId = new mongoose.Types.ObjectId();
  const invalidObjectId = 'invalid_object_id';

  // start the server before each test
  // and create a sender, a receiver, and a chat
  beforeEach(async () => {
    server = require('../../../app');
    
    sender = await User.create({
      username: 'john',
      password: 'mySecretPassword'
    });

    receiver = await User.create({
      username: 'jane',
      password: 'mySecretPassword'
    });

    chat = await Chat.create({
      members: [
        {
          user: sender._id.toString(),
          msgDestructionOpt: sender.defaultMsgDestruction
        },
        {
          user: receiver._id.toString(),
          msgDestructionOpt: receiver.defaultMsgDestruction
        }
      ]
    });
  });

  // clean up the test db and close server after each test
  afterEach(async () => {
    await Message.deleteMany({});
    await Chat.deleteMany({});
    await User.deleteMany({});
    await server.close();
  });

  describe('update user socket id', () => {
    it('should update a users socket id in the DB', async () => {
      const payload = {
        userId: sender._id,
        socketId: 'sender123'
      };

      await updateUserSocketId(payload);

      const updatedUser = await User.findById(payload.userId);

      expect(updatedUser.socketId).toBe(payload.socketId);
    });

    it('should return falsy if user id is invalid', async () => {
      const payload = {
        userId: invalidObjectId,
        socketId: 'sender123'
      };

      const res = await updateUserSocketId(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if socket id is not a string', async () => {
      const payload = {
        userId: sender._id,
        socketId: {}
      };

      const res = await updateUserSocketId(payload);

      expect(res).toBeFalsy();
    });
  });

  describe('reset user socket id', () => {
    it('should set a users socket id to an empty string in the DB', async () => {
      await resetUserSocketId(sender._id);

      const updatedUser = await User.findById(sender._id);

      expect(updatedUser.socketId).toBe('');
    });

    it('should return falsy if user id is invalid', async () => {
      const res = await resetUserSocketId(invalidObjectId);

      expect(res).toBeFalsy();
    });
  });

  describe('load settings', () => {
    it('should return the account settings for a valid user', async () => {
      const settings = await loadSettings(sender._id);
      
      const userFromDB = await User.findById(sender._id);

      expect(settings).toHaveProperty('defaultMsgDestruction', userFromDB.defaultMsgDestruction);
      expect(settings).toHaveProperty('decryptMsgByDefault', userFromDB.decryptMsgByDefault);
      expect(settings).toHaveProperty('newMsgNotificationOn', userFromDB.newMsgNotificationOn);
    });

    it('should return falsy if user does not exist', async () => {
      const res = await loadSettings(nonExistentId);

      expect(res).toBeFalsy();
    });

    it('should return falsy if user id is invalid', async () => {
      const res = await loadSettings(invalidObjectId);

      expect(res).toBeFalsy();
    });
  });

  describe('create chat', () => {
    it('should create new chat doc if members are valid and chat does not exist', async () => {
      // remove previously created chats
      await Chat.deleteMany({});

      chat = await createChat({
        senderId: sender._id,
        receiverId: receiver._id
      });
      const chatFromDB = await Chat.findById(chat._id);

      expect(chat).not.toBeNull();
      expect(chatFromDB).not.toBeNull();
    });

    it('should create chat doc with two members if they are valid and chat does not exist', async () => {
      // remove previously created chats
      await Chat.deleteMany({});
      
      chat = await createChat({
        senderId: sender._id,
        receiverId: receiver._id
      });
      const chatFromDB = await Chat.findById(chat._id);

      expect(chatFromDB.members).toHaveLength(2);
    });

    it('should add the new chat to the senders chat list', async () => {
      // remove previously created chats
      await Chat.deleteMany({});
      
      chat = await createChat({
        senderId: sender._id,
        receiverId: receiver._id
      });

      const senderFromDB = await User.findOne({ username: sender.username });
      const resChatId = senderFromDB.chats[0]._id.toString();
      const expectedChatId = chat._id.toString();

      expect(senderFromDB.chats).toHaveLength(1);
      expect(resChatId).toBe(expectedChatId);
    });

    it('should return chat if chat between the same members already exists', async () => {
      const existingChatId = chat._id;
      
      chat = await createChat({
        senderId: sender._id,
        receiverId: receiver._id
      });

      expect(chat).toHaveProperty('_id', existingChatId);
    });

    it('should add existing chat to senders chat list if sender does not have it', async () => {
      const existingChatId = chat._id;

      const chatCountBefore = sender.chats.length;
      
      chat = await createChat({
        senderId: sender._id,
        receiverId: receiver._id
      });

      const updatedUser = await User.findById(sender._id);
      const chatCountAfter = updatedUser.chats.length;
      const chatIsFound = updatedUser.chats.some(
        c => c._id.toString() === existingChatId.toString()
      );

      expect(chatCountAfter).toBe(chatCountBefore + 1);
      expect(chatIsFound).toBe(true);
    });

    it('should not create a new chat doc if sender does not exist', async () => {
      const countBefore = await Chat.countDocuments({});

      const res = await createChat({
        senderId: nonExistentId,
        receiverId: receiver._id
      });

      const countAfter = await Chat.countDocuments({});

      expect(res).toBeFalsy();
      expect(countBefore).toBe(countAfter);
    });

    it('should not create a new chat doc if receiver does not exist', async () => {
      const countBefore = await Chat.countDocuments({});

      const res = await createChat({
        senderId: sender._id,
        receiverId: nonExistentId
      });

      const countAfter = await Chat.countDocuments({});

      expect(res).toBeFalsy();
      expect(countBefore).toBe(countAfter);
    });

    it('should return falsy if sender id is invalid', async () => {
      const res = await createChat({
        senderId: invalidObjectId,
        receiverId: receiver._id
      });

      expect(res).toBeFalsy();
    });

    it('should return falsy if receiver id is invalid', async () => {
      const res = await createChat({
        senderId: sender._id,
        receiverId: invalidObjectId
      });

      expect(res).toBeFalsy();
    });
  });

  describe('load chats', () => {
    it('should return all chats in a valid users chat list', async () => {
      sender.chats.push(chat);
      await sender.save();

      const chats = await loadChats(sender._id);

      expect(chats).toHaveLength(1);
      expect(chats[0]).toHaveProperty('_id', chat._id);
    });

    it('should return an empty array if valid user does not have any chats', async () => {
      const res = await loadChats(sender._id);

      expect(sender.chats).toHaveLength(0);
      expect(res).toStrictEqual([]);
    });

    it('should return falsy if user does not exist', async () => {
      const res = await loadChats(nonExistentId);

      expect(res).toBeFalsy();
    });

    it('should return falsy if user id is invalid', async () => {
      const res = await loadChats(invalidObjectId);

      expect(res).toBeFalsy();
    });
  });

  describe('load unread messages', () => {
    it('should return a users unread messages', async () => {
      // 2 messages that HAVE NOT been read
      const msg1 = {
        _id: new mongoose.Types.ObjectId(),
        sender,
        receiver,
        msgEncryptedForSender: 'msgEncryptedForSender',
        msgEncryptedForReceiver: 'msgEncryptedForReceiver',
        hasBeenRead: false,
        expireAt: Date.now()
      };

      const msg2 = {
        _id: new mongoose.Types.ObjectId(),
        sender,
        receiver,
        msgEncryptedForSender: 'msgEncryptedForSender',
        msgEncryptedForReceiver: 'msgEncryptedForReceiver',
        hasBeenRead: false,
        expireAt: Date.now()
      };

      // 1 message that HAS been read
      const msg3 = {
        _id: new mongoose.Types.ObjectId(),
        sender,
        receiver,
        msgEncryptedForSender: 'msgEncryptedForSender',
        msgEncryptedForReceiver: 'msgEncryptedForReceiver',
        hasBeenRead: true,
        expireAt: Date.now()
      };

      await Message.insertMany([ msg1, msg2, msg3 ]);
      chat.messages.push(msg1, msg2, msg3);
      await chat.save();
      receiver.chats.push(chat);
      await receiver.save();

      const payload = {
        userId: receiver._id,
        chats: receiver.chats
      };
      const unreadMsgData = await loadUnreadMsgs(payload);

      expect(unreadMsgData).toHaveLength(1);
      expect(unreadMsgData[0]).toHaveProperty('chatId', chat._id);
      expect(unreadMsgData[0]).toHaveProperty('unreadMsgIds');
      expect(unreadMsgData[0].unreadMsgIds).toHaveLength(2);
      expect(unreadMsgData[0].unreadMsgIds).toContainEqual(msg1._id);
      expect(unreadMsgData[0].unreadMsgIds).toContainEqual(msg2._id);
    });

    it('should return an empty array if no unread messages', async () => {
      // 1 message that HAS been read
      const msg = {
        _id: new mongoose.Types.ObjectId(),
        sender,
        receiver,
        msgEncryptedForSender: 'msgEncryptedForSender',
        msgEncryptedForReceiver: 'msgEncryptedForReceiver',
        hasBeenRead: true,
        expireAt: Date.now()
      };

      await Message.insertMany([ msg ]);
      chat.messages.push(msg);
      await chat.save();
      receiver.chats.push(chat);
      await receiver.save();

      const payload = {
        userId: receiver._id,
        chats: receiver.chats
      };
      const unreadMsgData = await loadUnreadMsgs(payload);

      expect(unreadMsgData).toStrictEqual([]);
    });

    it('should return an empty array if user is the sender', async () => {
      // 1 message that HAS NOT been read
      const msg = {
        _id: new mongoose.Types.ObjectId(),
        sender,
        receiver,
        msgEncryptedForSender: 'msgEncryptedForSender',
        msgEncryptedForReceiver: 'msgEncryptedForReceiver',
        hasBeenRead: true,
        expireAt: Date.now()
      };

      await Message.insertMany([ msg ]);
      chat.messages.push(msg);
      await chat.save();
      sender.chats.push(chat);
      await sender.save();

      const payload = {
        userId: sender._id,
        chats: sender.chats
      };
      const unreadMsgData = await loadUnreadMsgs(payload);

      expect(unreadMsgData).toStrictEqual([]);
    });
    
    it('should return falsy if user does not exist', async () => {
      const payload = {
        userId: nonExistentId,
        chats: [ chat ]
      };

      const res = await loadUnreadMsgs(payload);

      expect(res).toBeFalsy();
    });
    
    it('should return falsy if user id is invalid', async () => {
      const payload = {
        userId: invalidObjectId,
        chats: [ chat ]
      };

      const res = await loadUnreadMsgs(payload);

      expect(res).toBeFalsy();
    });
    
    it('should return falsy if no chats are provided', async () => {
      const payload = {
        userId: receiver._id,
        chats: []
      };

      const res = await loadUnreadMsgs(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if chats arg is not an array', async () => {
      const payload = {
        userId: receiver._id,
        chats: 'not an array'
      };

      const res = await loadUnreadMsgs(payload);

      expect(res).toBeFalsy();
    });
  });

  describe('mark message as read', () => {
    it('should mark messages as read', async () => {
      // 2 messages that HAVE NOT been read
      const msg1 = {
        _id: new mongoose.Types.ObjectId(),
        sender,
        receiver,
        msgEncryptedForSender: 'msgEncryptedForSender',
        msgEncryptedForReceiver: 'msgEncryptedForReceiver',
        hasBeenRead: false,
        expireAt: Date.now()
      };

      const msg2 = {
        _id: new mongoose.Types.ObjectId(),
        sender,
        receiver,
        msgEncryptedForSender: 'msgEncryptedForSender',
        msgEncryptedForReceiver: 'msgEncryptedForReceiver',
        hasBeenRead: false,
        expireAt: Date.now()
      };

      await Message.insertMany([ msg1, msg2 ]);

      const payload = {
        unreadMsgIds: [ msg1._id, msg2._id ],
        userId: receiver._id
      };
      await markAsRead(payload);

      const updatedMsgs = await Message.find({ _id: { $in: payload.unreadMsgIds } });

      expect(updatedMsgs).toHaveLength(2);
      expect(updatedMsgs[0]).toHaveProperty('hasBeenRead', true);
      expect(updatedMsgs[1]).toHaveProperty('hasBeenRead', true);
    });

    it('should return true if successfully marked msgs as read', async () => {
      const msg = {
        _id: new mongoose.Types.ObjectId(),
        sender,
        receiver,
        msgEncryptedForSender: 'msgEncryptedForSender',
        msgEncryptedForReceiver: 'msgEncryptedForReceiver',
        hasBeenRead: false,
        expireAt: Date.now()
      };

      await Message.insertMany([ msg ]);

      const payload = {
        unreadMsgIds: [ msg._id ],
        userId: receiver._id
      };
      const res = await markAsRead(payload);

      expect(res).toBe(true);
    });

    it('should return falsy if no unread msgs ids are provided', async () => {
      const res = await markAsRead([]);

      expect(res).toBeFalsy();
    });

    it('should return falsy if the messages with the ids do not exist', async () => {
      const payload = {
        unreadMsgIds: [ nonExistentId ],
        userId: receiver._id
      };
      const res = await markAsRead(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if the user is not the receiver of the message', async () => {
      const msg = {
        _id: new mongoose.Types.ObjectId(),
        sender,
        receiver,
        msgEncryptedForSender: 'msgEncryptedForSender',
        msgEncryptedForReceiver: 'msgEncryptedForReceiver',
        hasBeenRead: false,
        expireAt: Date.now()
      };

      await Message.insertMany([ msg ]);

      const payload = {
        unreadMsgIds: [ msg._id ],
        userId: sender._id
      };
      const res = await markAsRead(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if user id is invalid', async () => {
      const payload = {
        unreadMsgIds: [ new mongoose.Types.ObjectId() ],
        userId: invalidObjectId
      };
      const res = await markAsRead(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if any of the message ids are invalid', async () => {
      const payload = {
        unreadMsgIds: [ invalidObjectId ],
        userId: sender._id
      };
      const res = await markAsRead(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if the message ids arg is not an array', async () => {
      const payload = {
        unreadMsgIds: 'not an array',
        userId: receiver._id
      };
      const res = await markAsRead(payload);

      expect(res).toBeFalsy();
    });
  });
  
  describe('set message destruction', () => {
    it('should set the msg destruction option for a user in a chat', async () => {
      const newDestructionOpt = expirations.ONE_MIN;
      const payload = {
        chatId: chat._id,
        userId: sender._id,
        destructionOpt: newDestructionOpt
      };

      await setMsgDestruction(payload);

      chat = await Chat.findById(payload.chatId);
      const updatedMember = chat.members.find(
        m => m.user.toString() === payload.userId.toString()
      );

      expect(updatedMember.msgDestructionOpt).toBe(payload.destructionOpt);
    });

    it('should return true if successfully setting msg desctruction opt', async () => {
      const payload = {
        chatId: chat._id,
        userId: sender._id,
        destructionOpt: expirations.ONE_MIN
      };

      const res = await setMsgDestruction(payload);

      expect(res).toBe(true);
    });

    it('should set msg destruction to the default if invalid option is provided', async () => {
      const invalidMsgDestructionOpt = '';
      const payload = {
        chatId: chat._id,
        userId: sender._id,
        destructionOpt: invalidMsgDestructionOpt
      };

      await setMsgDestruction(payload);

      chat = await Chat.findById(payload.chatId);
      const updatedMember = chat.members.find(
        m => m.user.toString() === payload.userId.toString()
      );

      expect(updatedMember.msgDestructionOpt).toBe(expirations.DEFAULT);
    });

    it('should return falsy if user does not exist', async () => {
      const payload = {
        chatId: chat._id,
        userId: nonExistentId,
        destructionOpt: expirations.DEFAULT
      };

      const res = await setMsgDestruction(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if chat does not exist', async () => {
      const payload = {
        chatId: nonExistentId,
        userId: sender._id,
        destructionOpt: expirations.DEFAULT
      };

      const res = await setMsgDestruction(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if chat id is invalid', async () => {
      const payload = {
        chatId: invalidObjectId,
        userId: sender._id,
        destructionOpt: expirations.DEFAULT
      };

      const res = await setMsgDestruction(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if user id is invalid', async () => {
      const payload = {
        chatId: chat._id,
        userId: invalidObjectId,
        destructionOpt: expirations.DEFAULT
      };

      const res = await setMsgDestruction(payload);

      expect(res).toBeFalsy();
    });
  });

  describe('create message', () => {
    it('should add a message to a chat in the DB if ids of chat, sender, receiver are valid', async () => {
      const payload = {
        chatId: chat._id,
        sender,
        receiver,
        msgEncryptedForSender: 'msgEncryptedForSender',
        msgEncryptedForReceiver: 'msgEncryptedForReceiver'
      };

      await createMsg(payload);

      chat = await Chat.findById(payload.chatId);

      expect(chat.messages).toHaveLength(1);
      expect(chat.messages[0]).toHaveProperty('_id');
    });

    it('should add a message in the DB if ids of chat, sender, receiver are valid', async () => {
      const payload = {
        chatId: chat._id,
        sender,
        receiver,
        msgEncryptedForSender: 'msgEncryptedForSender',
        msgEncryptedForReceiver: 'msgEncryptedForReceiver'
      };

      await createMsg(payload);

      chat = await Chat.findById(payload.chatId);
      const msg = await Message.findById(chat.messages[0]._id);

      expect(msg).not.toBeNull();
    });

    it('should return the created message if ids of chat, sender, receiver are valid', async () => {
      const payload = {
        chatId: chat._id,
        sender,
        receiver,
        msgEncryptedForSender: 'msgEncryptedForSender',
        msgEncryptedForReceiver: 'msgEncryptedForReceiver'
      };

      const { msg } = await createMsg(payload);

      expect(msg).toHaveProperty('_id');
      expect(msg).toHaveProperty('sender', sender._id);
      expect(msg).toHaveProperty('receiver', receiver._id);
      expect(msg).toHaveProperty('msgEncryptedForSender');
      expect(msg).toHaveProperty('msgEncryptedForReceiver');
    });

    it('should update the chats most recent message date if ids of chat, sender, receiver are valid', async () => {
      const payload = {
        chatId: chat._id,
        sender,
        receiver,
        msgEncryptedForSender: 'msgEncryptedForSender',
        msgEncryptedForReceiver: 'msgEncryptedForReceiver'
      };

      const { msg } = await createMsg(payload);

      chat = await Chat.findById(payload.chatId);

      expect(chat.mostRecentMsgDate.toString()).toBe(msg.createdAt.toString());
    });

    it('should return falsy if adding message to a non-existent chat', async () => {
      const payload = {
        chatId: nonExistentId,
        sender,
        receiver,
        msgEncryptedForSender: 'msgEncryptedForSender',
        msgEncryptedForReceiver: 'msgEncryptedForReceiver'
      };

      const res = await createMsg(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if sender does not exist', async () => {
      const payload = {
        chatId: chat._id,
        sender: { _id: nonExistentId },
        receiver,
        msgEncryptedForSender: 'msgEncryptedForSender',
        msgEncryptedForReceiver: 'msgEncryptedForReceiver'
      };

      const res = await createMsg(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if receiver does not exist', async () => {
      const payload = {
        chatId: chat._id,
        sender,
        receiver: { _id: nonExistentId },
        msgEncryptedForSender: 'msgEncryptedForSender',
        msgEncryptedForReceiver: 'msgEncryptedForReceiver'
      };

      const res = await createMsg(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if chat id is invalid', async () => {
      const payload = {
        chatId: { _id: invalidObjectId },
        sender,
        receiver,
        msgEncryptedForSender: 'msgEncryptedForSender',
        msgEncryptedForReceiver: 'msgEncryptedForReceiver'
      };

      const res = await createMsg(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if sender id is invalid', async () => {
      const payload = {
        chatId: chat._id,
        sender: { _id: invalidObjectId },
        receiver,
        msgEncryptedForSender: 'msgEncryptedForSender',
        msgEncryptedForReceiver: 'msgEncryptedForReceiver'
      };

      const res = await createMsg(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if receiver id is invalid', async () => {
      const payload = {
        chatId: chat._id,
        sender,
        receiver: { _id: invalidObjectId },
        msgEncryptedForSender: 'msgEncryptedForSender',
        msgEncryptedForReceiver: 'msgEncryptedForReceiver'
      };

      const res = await createMsg(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if chatId is falsy', async () => {
      const payload = {
        chatId: '',
        sender,
        receiver,
        msgEncryptedForSender: 'msgEncryptedForSender',
        msgEncryptedForReceiver: 'msgEncryptedForReceiver'
      };

      const res = await createMsg(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if sender is falsy', async () => {
      const payload = {
        chatId: chat._id,
        sender: '',
        receiver,
        msgEncryptedForSender: 'msgEncryptedForSender',
        msgEncryptedForReceiver: 'msgEncryptedForReceiver'
      };

      const res = await createMsg(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if receiver is falsy', async () => {
      const payload = {
        chatId: chat._id,
        sender,
        receiver: '',
        msgEncryptedForSender: 'msgEncryptedForSender',
        msgEncryptedForReceiver: 'msgEncryptedForReceiver'
      };

      const res = await createMsg(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if message is falsy', async () => {
      const payload = {
        chatId: chat._id,
        sender,
        receiver,
        msgEncryptedForSender: '',
        msgEncryptedForReceiver: ''
      };

      const res = await createMsg(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if message is not a string', async () => {
      const payload = {
        chatId: chat._id,
        sender,
        receiver,
        msgEncryptedForSender: {},
        msgEncryptedForReceiver: {}
      };

      const res = await createMsg(payload);

      expect(res).toBeFalsy();
    });
  });

  describe('load chat messages', () => {
    it('should return all chat messages in a valid chat', async () => {
      const msg = await Message.create({
        sender,
        receiver,
        msgEncryptedForSender: 'msgEncryptedForSender',
        msgEncryptedForReceiver: 'msgEncryptedForReceiver',
        expireAt: Date.now()
      });

      chat.messages.push(msg);
      await chat.save();

      const msgs = await loadChatMsgs(chat._id);

      expect(msgs).toHaveLength(1);
      expect(msgs[0]).toHaveProperty('_id', msg._id);
    });

    it('should return an empty array if there are no messages in the chat', async () => {
      const msgs = await loadChatMsgs(chat._id);

      expect(chat.messages).toHaveLength(0);
      expect(msgs).toStrictEqual([]);
    });

    it('should return falsy if chat does not exist', async () => {
      const res = await loadChatMsgs(nonExistentId);

      expect(res).toBeFalsy();
    });
    
    it('should return falsy if chat id is invalid', async () => {
      const res = await loadChatMsgs(invalidObjectId);

      expect(res).toBeFalsy();
    });
  });

  describe('get crypto data', () => {
    it('should return the encrypted private key and salt for valid user', async () => {
      const privKey = 'privKey';
      const salt = 'salt';

      await User.findByIdAndUpdate(sender._id, { privKey, salt });

      const cryptoData = await getCryptoData(sender._id);

      expect(cryptoData.privKey).toBe(privKey);
      expect(cryptoData.salt).toBe(salt);
    });

    it('should return falsy if user does not exist', async () => {
      const res = await getCryptoData(nonExistentId);

      expect(res).toBeFalsy();
    });

    it('should return falsy if user id is invalid', async () => {
      const res = await getCryptoData(invalidObjectId);

      expect(res).toBeFalsy();
    });
  });

  describe('store crypto data', () => {
    it('should update the crypto data for a valid user', async () => {
      const payload = {
        userId: sender._id,
        pubKey: 'pubKey',
        encrPrivKey: 'encrPrivKey',
        salt: 'salt'
      };

      await storeCryptoData(payload);

      const updatedUser = await User.findById(sender._id);

      expect(updatedUser.pubKey).toBe(payload.pubKey);
      expect(updatedUser.privKey).toBe(payload.encrPrivKey);
      expect(updatedUser.salt).toBe(payload.salt);
    });

    it('should return falsy if user id is invalid', async () => {
      const payload = {
        userId: invalidObjectId,
        pubKey: 'pubKey',
        encrPrivKey: 'encrPrivKey',
        salt: 'salt'
      };

      const res = await storeCryptoData(payload);

      expect(res).toBeFalsy();
    });
  });

  describe('load contacts', () => {
    it('should return all contacts in a valid users contact list', async () => {
      // add a contact to a user's list
      sender.contacts.push({ user: receiver._id });
      await sender.save();

      const contacts = await loadContacts(sender._id);

      expect(contacts).toHaveLength(1);
      expect(contacts[0]).toHaveProperty('_id', receiver._id);
    });

    it('should return _id, username, blocked, blockedByContact as fields', async () => {
      // add a contact to a user's list
      sender.contacts.push({ user: receiver._id });
      await sender.save();

      const contacts = await loadContacts(sender._id);

      expect(contacts[0]).toHaveProperty('_id');
      expect(contacts[0]).toHaveProperty('username');
      expect(contacts[0]).toHaveProperty('blocked');
      expect(contacts[0]).toHaveProperty('blockedByContact');
    });

    it('should return an empty array if user has no contacts', async () => {
      const contacts = await loadContacts(sender._id);

      expect(sender.contacts).toHaveLength(0);
      expect(contacts).toStrictEqual([]);
    });

    it('should remove contact from list if contact has removed its account', async () => {
      // add a contact to a user's list
      sender.contacts.push({ user: receiver._id });
      await sender.save();

      // simulate the contact removing its account
      await User.deleteOne({ _id: receiver._id });

      // load the user's contacts
      await loadContacts(sender._id);

      // check that the user's contact list does not contain the contact anymore
      const senderFromDB = await User.findById(sender._id);
      const contactHasBeenRemoved = senderFromDB.contacts.every(
        c => c.user.toString() !== receiver._id.toString()
      );

      expect(contactHasBeenRemoved).toBe(true);
    });

    it('should return falsy if user does not exist', async () => {
      const res = await loadContacts(nonExistentId);

      expect(res).toBeFalsy();
    });

    it('should return falsy if user id is invalid', async () => {
      const res = await loadContacts(invalidObjectId);

      expect(res).toBeFalsy();
    });
  });

  describe('store contact', () => {
    it('should add a valid contact to a valid users contact list in the DB', async () => {
      const countBefore = sender.contacts.length;

      const payload = {
        userId: sender._id,
        contactId: receiver._id
      };
      await storeContact(payload);

      const senderFromDB = await User.findById(sender._id);
      const countAfter = senderFromDB.contacts.length;

      expect(countAfter).toBe(countBefore + 1);
      expect(senderFromDB.contacts[0]).toHaveProperty('user', receiver._id);
    });

    it('should return true when contact is added to a users contact list', async () => {
      const payload = {
        userId: sender._id,
        contactId: receiver._id
      };
      const res = await storeContact(payload);

      expect(res).toBe(true);
    });

    it('should return falsy if contact has already been stored', async () => {
      // store the contact manually first
      const contactToBeStored = receiver._id;
      sender.contacts.push({ user: contactToBeStored });
      await sender.save();

      // try storing it again
      const payload = {
        userId: sender._id,
        contactId: contactToBeStored
      };
      const res = await storeContact(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if user does not exist', async () => {
      const payload = {
        userId: nonExistentId,
        contactId: receiver._id
      };
      const res = await storeContact(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if contact to be added does not exist', async () => {
      const payload = {
        userId: sender._id,
        contactId: nonExistentId
      };
      const res = await storeContact(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if user id is invalid', async () => {
      const payload = {
        userId: invalidObjectId,
        contactId: receiver._id
      };
      const res = await storeContact(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if contact id is invalid', async () => {
      const payload = {
        userId: sender._id,
        contactId: invalidObjectId
      };
      const res = await storeContact(payload);

      expect(res).toBeFalsy();
    });
  });

  describe('load search result', () => {

    const COMMON_SEQUENCE = 'abc';
    const NO_COMMON_SEQUENCE = 'zzz';

    const username1 = COMMON_SEQUENCE + 'def';
    const username2 = 'def' + COMMON_SEQUENCE;
    const username3 = 'd' + COMMON_SEQUENCE + 'ef';
    const username4 = 'unique';

    const insertUsers = () => User.insertMany([
      { username: username1, password: 'mySecretPassword' },
      { username: username2, password: 'mySecretPassword' },
      { username: username3, password: 'mySecretPassword' },
      { username: username4, password: 'mySecretPassword' },
    ]);

    it('should return all usernames with a specified sequence', async () => {
      // create users with usernames that contain the sequence "abc"
      await insertUsers();

      const payload = {
        usernamePrefix: COMMON_SEQUENCE,
        searcherId: sender._id
      };
      const res = await loadSearchRes(payload);

      expect(res).toHaveLength(3);
      expect(res.some(user => user.username === username1)).toBe(true);
      expect(res.some(user => user.username === username2)).toBe(true);
      expect(res.some(user => user.username === username3)).toBe(true);
      expect(res.some(user => user.username === username4)).toBe(false);
    });

    it('should return all usernames with a specified sequence, case-insensitive', async () => {
      await insertUsers();

      const payload = {
        usernamePrefix: COMMON_SEQUENCE.toUpperCase(),
        searcherId: sender._id
      };
      const res = await loadSearchRes(payload);

      expect(res).toHaveLength(3);
      expect(res.some(user => user.username === username1)).toBe(true);
      expect(res.some(user => user.username === username2)).toBe(true);
      expect(res.some(user => user.username === username3)).toBe(true);
      expect(res.some(user => user.username === username4)).toBe(false);
    });

    it('should return search result sorted by username', async () => {
      await insertUsers();

      const payload = {
        usernamePrefix: COMMON_SEQUENCE,
        searcherId: sender._id
      };
      const res = await loadSearchRes(payload);

      expect(res).toHaveLength(3);
      expect(res[0].username).toBe(username1);
      expect(res[1].username).toBe(username3);
      expect(res[2].username).toBe(username2);
    });

    it('should return _id, username, blocked, blockedByContact as fields', async () => {
      await insertUsers();

      const payload = {
        usernamePrefix: COMMON_SEQUENCE,
        searcherId: sender._id
      };
      const res = await loadSearchRes(payload);

      expect(res[0]).toHaveProperty('_id');
      expect(res[0]).toHaveProperty('username');
      expect(res[0]).toHaveProperty('blocked');
      expect(res[0]).toHaveProperty('blockedByContact');
    });

    it('should return an empty array if no usernames with the sequence exist', async () => {
      await insertUsers();

      const payload = {
        usernamePrefix: NO_COMMON_SEQUENCE,
        searcherId: sender._id
      };
      const res = await loadSearchRes(payload);

      expect(res).toStrictEqual([]);
    });

    it('should return an empty array if search value is falsy', async () => {
      const payload = {
        usernamePrefix: '',
        searcherId: sender._id
      };
      const res = await loadSearchRes(payload);

      expect(res).toStrictEqual([]);
    });

    it('should return an empty array if search value length is less than 3', async () => {
      const payload = {
        usernamePrefix: 'ab',
        searcherId: sender._id
      };
      const res = await loadSearchRes(payload);

      expect(res).toStrictEqual([]);
    });

    it('should not return the searcher itself even if username matches', async () => {
      const payload = {
        usernamePrefix: sender.username,
        searcherId: sender._id
      };
      const res = await loadSearchRes(payload);

      expect(res).toStrictEqual([]);
    });

    it('should return falsy if searcher id is invalid', async () => {
      const payload = {
        usernamePrefix: COMMON_SEQUENCE,
        searcherId: invalidObjectId
      };
      const res = await loadSearchRes(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if username prefix is not a string', async () => {
      const payload = {
        usernamePrefix: {},
        searcherId: sender._id
      };
      const res = await loadSearchRes(payload);

      expect(res).toBeFalsy();
    });
  });

  describe('block contact', () => {
    it('should add the contact to the blocked list if valid ids', async () => {
      const payload = {
        userId: sender._id,
        contactId: receiver._id
      };
      await blockContact(payload);

      const user = await User.findById(payload.userId);
      const contactIsBlocked = user.blockedUsers.some(
        bu => bu.toString() === payload.contactId.toString()
      );

      expect(contactIsBlocked).toBe(true);
    });

    it('should return falsy if contact is already blocked', async () => {
      sender.blockedUsers.push(receiver._id);
      await sender.save();
      
      const payload = {
        userId: sender._id,
        contactId: receiver._id
      };
      const res = await blockContact(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if user does not exist', async () => {
      const payload = {
        userId: nonExistentId,
        contactId: receiver._id
      };
      const res = await blockContact(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if contact does not exist', async () => {
      const payload = {
        userId: sender._id,
        contactId: nonExistentId
      };
      const res = await blockContact(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if user id is invalid', async () => {
      const payload = {
        userId: invalidObjectId,
        contactId: receiver._id
      };
      const res = await blockContact(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if contact id is invalid', async () => {
      const payload = {
        userId: sender._id,
        contactId: invalidObjectId
      };
      const res = await blockContact(payload);

      expect(res).toBeFalsy();
    });
  });

  describe('unblock contact', () => {
    it('should remove the contact from the blocked list if valid ids', async () => {
      sender.blockedUsers.push(receiver._id);
      await sender.save();

      const payload = {
        userId: sender._id,
        contactId: receiver._id
      };
      await unblockContact(payload);

      const user = await User.findById(payload.userId);
      const contactIsBlocked = user.blockedUsers.some(
        bu => bu.toString() === payload.contactId.toString()
      );

      expect(contactIsBlocked).toBe(false);
    });

    it('should return falsy if contact is not blocked', async () => {
      const payload = {
        userId: sender._id,
        contactId: receiver._id
      };
      const res = await unblockContact(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if user does not exist', async () => {
      const payload = {
        userId: nonExistentId,
        contactId: receiver._id
      };
      const res = await unblockContact(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if contact does not exist', async () => {
      const payload = {
        userId: sender._id,
        contactId: nonExistentId
      };
      const res = await unblockContact(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if user id is invalid', async () => {
      const payload = {
        userId: invalidObjectId,
        contactId: receiver._id
      };
      const res = await unblockContact(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if contact id is invalid', async () => {
      const payload = {
        userId: sender._id,
        contactId: invalidObjectId
      };
      const res = await unblockContact(payload);

      expect(res).toBeFalsy();
    });
  });

  describe('delete contact', () => {
    it('should delete the contact from the users contact list if both ids are valid', async () => {
      // add a valid contact to a valid user's contact list
      sender.contacts.push({ user: receiver });
      await sender.save();

      const contactCountBefore = sender.contacts.length;

      const payload = {
        userId: sender._id,
        contactId: receiver._id
      };
      await deleteContact(payload);

      const updatedUser = await User.findById(payload.userId);
      const contactCountAfter = updatedUser.contacts.length;
      const contactIsFound = updatedUser.contacts.some(
        c => c.user.toString() === payload.contactId.toString()
      );

      expect(contactCountAfter).toBe(contactCountBefore - 1);
      expect(contactIsFound).toBe(false);
    });

    it('should return true if contact is successfully deleted', async () => {
      // add a valid contact to a valid user's contact list
      sender.contacts.push({ user: receiver });
      await sender.save();

      const payload = {
        userId: sender._id,
        contactId: receiver._id
      };
      const res = await deleteContact(payload);

      expect(res).toBe(true);
    });

    it('should delete their chat if user has their chat in its chat list', async () => {
      // add a valid contact to a valid user's contact list
      // and add their chat to the user's chat list
      sender.contacts.push({ user: receiver });
      sender.chats.push(chat);
      await sender.save();

      const payload = {
        userId: sender._id,
        contactId: receiver._id
      };
      await deleteContact(payload);

      const updatedUser = await User.findById(payload.userId);
      const chatIsFound = updatedUser.chats.some(
        c => c._id.toString() === chat._id.toString()
      );

      expect(chatIsFound).toBe(false);
    });

    it('should return falsy if user does not exist', async () => {
      const payload = {
        userId: nonExistentId,
        contactId: receiver._id
      };
      const res = await deleteContact(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if user id is invalid', async () => {
      const payload = {
        userId: invalidObjectId,
        contactId: receiver._id
      };
      const res = await deleteContact(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if contact id is invalid', async () => {
      const payload = {
        userId: sender._id,
        contactId: invalidObjectId
      };
      const res = await deleteContact(payload);

      expect(res).toBeFalsy();
    });
  });

  describe('delete chat', () => {

    const insertChatToSenderChatList = () => {
      sender.chats.push(chat);
      sender.save();
    };

    const insertChatToReceiverChatList = () => {
      receiver.chats.push(chat);
      receiver.save();
    };

    it('should delete the chat from a users chat list if both are valid', async () => {
      // add a valid chat to a valid user's chat list
      await insertChatToSenderChatList();

      const chatCountBefore = sender.chats.length;

      const payload = {
        userId: sender._id,
        chatId: chat._id
      };
      await deleteChat(payload);

      const updatedUser = await User.findById(payload.userId);
      const chatCountAfter = updatedUser.chats.length;
      const chatIsFound = updatedUser.chats.some(
        c => c._id.toString() === payload.chatId.toString()
      );

      expect(chatCountAfter).toBe(chatCountBefore - 1);
      expect(chatIsFound).toBe(false);
    });

    it('should return true if chat is succesfully deleted from a users list', async () => {
      // add a valid chat to a valid user's chat list
      await insertChatToSenderChatList();

      const payload = {
        userId: sender._id,
        chatId: chat._id
      };
      const res = await deleteChat(payload);

      expect(res).toBe(true);
    });

    it('should delete chat doc from DB if chat partner does not have the chat in its list', async () => {
      // add a valid chat to only one of the chat member's chat list
      await insertChatToSenderChatList();

      const payload = {
        userId: sender._id,
        chatId: chat._id
      };

      const chatBefore = await Chat.findById(payload.chatId);

      await deleteChat(payload);

      const chatAfter = await Chat.findById(payload.chatId);

      expect(chatBefore).not.toBeNull();
      expect(chatBefore).toHaveProperty('_id');
      expect(chatAfter).toBeNull();
    });

    it('should not delete chat doc from DB if chat partner has the chat in its list', async () => {
      // add a valid chat to both chat members' chat lists
      await insertChatToSenderChatList();
      await insertChatToReceiverChatList();

      const payload = {
        userId: sender._id,
        chatId: chat._id
      };
      await deleteChat(payload);

      chat = await Chat.findById(payload.chatId);

      expect(chat).not.toBeNull();
      expect(chat).toHaveProperty('_id');
    });

    it('should delete chat msgs from DB if chat partner does not have the chat in its list', async () => {
      // add a message to the chat
      const msg = await Message.create({
        sender,
        receiver,
        msgEncryptedForSender: 'msgEncryptedForSender',
        msgEncryptedForReceiver: 'msgEncryptedForReceiver',
        expireAt: Date.now()
      });
      chat.messages.push(msg);
      await chat.save();
      
      // add the chat to only one of the chat member's chat list
      await insertChatToSenderChatList();

      const msgCountBefore = await Message.countDocuments({});
      const msgBefore = await Message.findById(msg._id);

      const payload = {
        userId: sender._id,
        chatId: chat._id
      };
      await deleteChat(payload);

      const msgCountAfter = await Message.countDocuments({});
      const msgAfter = await Message.findById(msg._id);

      expect(msgBefore).not.toBeNull();
      expect(msgCountAfter).toBe(msgCountBefore - 1);
      expect(msgAfter).toBeNull();
    });

    it('should not delete chat msgs from DB if chat partner has the chat in its list', async () => {
      // add a message to the chat
      const msg = await Message.create({
        sender,
        receiver,
        msgEncryptedForSender: 'msgEncryptedForSender',
        msgEncryptedForReceiver: 'msgEncryptedForReceiver',
        expireAt: Date.now()
      });
      chat.messages.push(msg);
      await chat.save();
      
      // add the chat to both chat members' chat lists
      await insertChatToSenderChatList();
      await insertChatToReceiverChatList();

      const msgCountBefore = await Message.countDocuments({});
      const msgBefore = await Message.findById(msg._id);

      const payload = {
        userId: sender._id,
        chatId: chat._id
      };
      await deleteChat(payload);

      const msgCountAfter = await Message.countDocuments({});
      const msgAfter = await Message.findById(msg._id);

      expect(msgBefore).not.toBeNull();
      expect(msgCountAfter).toBe(msgCountBefore);
      expect(msgAfter).not.toBeNull();
      expect(msgAfter).toHaveProperty('_id');
    });

    it('should return falsy if user does not exist', async () => {
      const payload = {
        userId: nonExistentId,
        chatId: chat._id
      };
      const res = await deleteChat(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if chat does not exist', async () => {
      const payload = {
        userId: sender._id,
        chatId: nonExistentId
      };
      const res = await deleteChat(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if user id is invalid', async () => {
      const payload = {
        userId: invalidObjectId,
        chatId: chat._id
      };
      const res = await deleteChat(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if chat id is invalid', async () => {
      const payload = {
        userId: sender._id,
        chatId: invalidObjectId
      };
      const res = await deleteChat(payload);

      expect(res).toBeFalsy();
    });
  });

  describe('update the default-message-destruction setting', () => {
    it('should update a valid users default message destruction', async () => {
      const payload = {
        userId: sender._id,
        defaultMsgDestruction: expirations.ONE_HOUR
      };
      await updateDefaultMsgDestruction(payload);

      const userFromDB = await User.findById(payload.userId);

      expect(userFromDB.defaultMsgDestruction).toBe(payload.defaultMsgDestruction);
    });

    it('should set the destruction option to the default if invalid option provided', async () => {
      const payload = {
        userId: sender._id,
        defaultMsgDestruction: 'invalid option'
      };
      await updateDefaultMsgDestruction(payload);

      const userFromDB = await User.findById(payload.userId);

      expect(userFromDB.defaultMsgDestruction).toBe(expirations.DEFAULT);
    });

    it('should return falsy if user id is invalid', async () => {
      const payload = {
        userId: invalidObjectId,
        defaultMsgDestruction: expirations.ONE_HOUR
      };
      const res = await updateDefaultMsgDestruction(payload);

      expect(res).toBeFalsy();
    });
  });

  describe('update the decrypt-message-by-default setting', () => {
    it('should update a valid users decrypt-message-by-default', async () => {
      const payload = {
        userId: sender._id,
        decryptMsgByDefault: false
      };
      await updateDecryptMsgByDefault(payload);

      const userFromDB = await User.findById(payload.userId);

      expect(userFromDB.decryptMsgByDefault).toBe(payload.decryptMsgByDefault);
    });

    it('should not update user if option provided is not a boolean', async () => {
      const payload = {
        userId: sender._id,
        decryptMsgByDefault: 'non-boolean value'
      };
      await updateDecryptMsgByDefault(payload);

      const userFromDB = await User.findById(payload.userId);

      expect(userFromDB.decryptMsgByDefault).not.toBe(payload.decryptMsgByDefault);
    });

    it('should return falsy if option provided is not a boolean', async () => {
      const payload = {
        userId: sender._id,
        decryptMsgByDefault: 'non-boolean value'
      };
      const res = await updateDecryptMsgByDefault(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if user id is invalid', async () => {
      const payload = {
        userId: invalidObjectId,
        decryptMsgByDefault: false
      };
      const res = await updateDecryptMsgByDefault(payload);

      expect(res).toBeFalsy();
    });
  });

  describe('update the new-message-notification-on setting', () => {
    it('should update a valid users new-message-notification-on', async () => {
      const payload = {
        userId: sender._id,
        newMsgNotificationOn: false
      };
      await updateNewMsgNotification(payload);

      const userFromDB = await User.findById(payload.userId);

      expect(userFromDB.newMsgNotificationOn).toBe(payload.newMsgNotificationOn);
    });

    it('should not update user if option provided is not a boolean', async () => {
      const payload = {
        userId: sender._id,
        newMsgNotificationOn: 'non-boolean value'
      };
      await updateNewMsgNotification(payload);

      const userFromDB = await User.findById(payload.userId);

      expect(userFromDB.newMsgNotificationOn).not.toBe(payload.newMsgNotificationOn);
    });

    it('should return falsy if option provided is not a boolean', async () => {
      const payload = {
        userId: sender._id,
        newMsgNotificationOn: false
      };
      const res = await updateNewMsgNotification(payload);

      expect(res).toBeFalsy();
    });

    it('should return falsy if user id is invalid', async () => {
      const payload = {
        userId: invalidObjectId,
        newMsgNotificationOn: false
      };
      const res = await updateNewMsgNotification(payload);

      expect(res).toBeFalsy();
    });
  });

  describe('delete account', () => {

    const correctPassword = 'mySecretPassword';
  
    const createUser = async () => {
      return User.create({
        username: 'user',
        password: await encryptPW(correctPassword)
      });
    };

    it('should delete the users account if valid id, password, and token', async () => {
      const user = await createUser();
      const token = user.generateAuthToken();

      const payload = {
        userId: user._id,
        password: correctPassword,
        token
      };
      await deleteAccount(payload);

      const userFromDB = await User.findById(payload.userId);

      expect(userFromDB).toBeFalsy();
    });

    it('should return a success object if deleting was successful', async () => {
      const user = await createUser();
      const token = user.generateAuthToken();

      const payload = {
        userId: user._id,
        password: correctPassword,
        token
      };
      const res = await deleteAccount(payload);

      expect(res).toHaveProperty('success', true);
    });

    it('should delete the users chats if valid id, password, and token', async () => {
      const user = await createUser();
      const token = user.generateAuthToken();
      chat = await Chat.create({
        members: [
          {
            user: user._id.toString(),
            msgDestructionOpt: user.defaultMsgDestruction
          },
          {
            user: receiver._id.toString(),
            msgDestructionOpt: receiver.defaultMsgDestruction
          }
        ]
      });
      user.chats.push(chat);
      await user.save();

      const payload = {
        userId: user._id,
        password: correctPassword,
        token
      };
      await deleteAccount(payload);

      const chatFromDB = await Chat.findById(chat._id);

      expect(chatFromDB).toBeFalsy();
    });

    it('should return an error object if user id is invalid', async () => {
      const user = await createUser();
      const token = user.generateAuthToken();

      const payload = {
        userId: invalidObjectId,
        password: correctPassword,
        token
      };
      const res = await deleteAccount(payload);

      expect(res).toHaveProperty('error');
      expect(res.error).toHaveProperty('message');
    });

    it('should return an error object if password is falsy', async () => {
      const user = await createUser();
      const token = user.generateAuthToken();

      const payload = {
        userId: user._id,
        password: undefined,
        token
      };
      const res = await deleteAccount(payload);

      expect(res).toHaveProperty('error');
      expect(res.error).toHaveProperty('message');
    });

    it('should return an error object if token is falsy', async () => {
      const user = await createUser();

      const payload = {
        userId: user._id,
        password: correctPassword,
        token: undefined
      };
      const res = await deleteAccount(payload);

      expect(res).toHaveProperty('error');
      expect(res.error).toHaveProperty('message');
    });

    it('should return an error object if user does not exist', async () => {
      const payload = {
        userId: nonExistentId,
        password: 'password',
        token: 'token'
      };
      const res = await deleteAccount(payload);

      expect(res).toHaveProperty('error');
      expect(res.error).toHaveProperty('message');
    });

    it('should return an error object if password is incorrect', async () => {
      const user = await createUser();
      const token = user.generateAuthToken();
      
      const payload = {
        userId: user._id,
        password: 'incorrect_password',
        token
      };
      const res = await deleteAccount(payload);

      expect(res).toHaveProperty('error');
      expect(res.error).toHaveProperty('message');
    });

    it('should return an error object if the token is invalid', async () => {
      const user = await createUser();
      
      const payload = {
        userId: user._id,
        password: correctPassword,
        token: 'invalid_token'
      };
      const res = await deleteAccount(payload);

      expect(res).toHaveProperty('error');
      expect(res.error).toHaveProperty('message');
    });
  });
});
