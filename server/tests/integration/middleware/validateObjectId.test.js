const request = require('supertest');
const { User } = require('../../../models/user');

describe('validate objectId middleware', () => {

  let server;
  let token;
  let objectId;

  // start the server and generate objectId before each test
  beforeEach(async () => {
    server = require('../../../app');
    const user = await User.create({
      username: 'john',
      password: 'mySecretPassword'
    });

    token = user.generateAuthToken();
    objectId = user._id;
  });

  // clean up the test db and close server after each test
  afterEach(async () => {
    await User.deleteMany({});
    await server.close();
  });

  // server request
  const API_ENDPOINT = '/api/v1/users/id';
  const exec = () => (
    request(server)
      .get(API_ENDPOINT + '/' + objectId)
      .set('Authorization', `bearer ${token}`)
  );

  it('should return 200 if objectId is valid', async () => {
    const res = await exec();

    expect(res.status).toBe(200);
  });

  it('should return 400 if objectId is invalid', async () => {
    objectId = 'invalidObjectId';

    const res = await exec();

    expect(res.status).toBe(400);
  });
});
