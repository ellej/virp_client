const request = require('supertest');
const { User } = require('../../../models/user');

describe('auth middleware', () => {

  let server;
  let token;
  let username = 'john';

  // start the server and generate token before each test
  beforeEach(async () => {
    server = require('../../../app');
    const user = await User.create({
      username,
      password: 'mySecretPassword'
    });

    token = user.generateAuthToken();
  });

  // clean up the test db and close server after each test
  afterEach(async () => {
    await User.deleteMany({});
    await server.close();
  });

  // server request
  const API_ENDPOINT = `/api/v1/users/username/${username}`;
  const exec = () => (
    request(server)
      .get(API_ENDPOINT)
      .set('Authorization', `bearer ${token}`)
  );

  it('should return 200 if token is valid', async () => {
    const res = await exec();

    expect(res.status).toBe(200);
  });

  it('should return 401 if no token is provided', async () => {
    // make request without setting the auth header
    const res = await request(server).get(API_ENDPOINT);

    expect(res.status).toBe(401);
  });

  it('should return 401 if token is invalid', async () => {
    token = 'invalidToken';

    const res = await exec();

    expect(res.status).toBe(401);
  });
});
