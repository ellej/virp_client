const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { USER_COLLECTION, CHAT_COLLECTION } = require('../utils/collections');
const { expirations } = require('../utils/msgExpirations');

const userSchema = new Schema({
  username: {
    type: String,
    minlength: 4,
    maxlength: 50,
    lowercase: true,
    trim: true,
    unique: true,
    required: true
  },
  password: {
    type: String,
    minlength: 7,
    maxlength: 1024,
    required: true
  },
  privKey: {
    type: String,
    maxlength: 3000
  },
  pubKey: {
    type: String,
    maxlength: 1024
  },
  salt: {
    type: String,
    maxlength: 1024
  },
  socketId: {
    type: String,
    trim: true
  },
  chats: [{
    type: Schema.Types.ObjectId,
    ref: CHAT_COLLECTION
  }],
  contacts: [{
    user: {
      type: Schema.Types.ObjectId,
      ref: USER_COLLECTION
    }
  }],
  blockedUsers: [{
    type: Schema.Types.ObjectId,
    ref: USER_COLLECTION
  }],
  defaultMsgDestruction: {
    type: String,
    trim: true,
    lowercase: true,
    enum: Object.values(expirations),
    default: expirations.DEFAULT
  },
  decryptMsgByDefault: {
    type: Boolean,
    default: true
  },
  newMsgNotificationOn: {
    type: Boolean,
    default: true
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
});

userSchema.methods = {
  generateAuthToken: function() {
    const user = {
      _id: this._id,
      username: this.username,
      pubKey: this.pubKey
    };

    return jwt.sign(user, process.env.JWT_SECRET_KEY, { expiresIn: '1h' });
  },
  comparePW: async function(plainTextPW) {
    const isMatch = await bcrypt.compare(plainTextPW, this.password);

    return isMatch;
  }
};

userSchema.index({ username: 'text' });

const User = mongoose.model(USER_COLLECTION, userSchema);

async function encryptPW(pw) {
  const SALT_ROUNDS = 10;
  const salt = await bcrypt.genSalt(SALT_ROUNDS);
  const encrypted = await bcrypt.hash(pw, salt);

  return encrypted;
}

function validate(user) {
  const schema = Joi.object({
    username: Joi.string().lowercase().trim().min(4).max(50).required(),
    password: Joi.string().min(7).max(1024).required().strict(),
    privKey: Joi.string().min(1).max(3000),
    pubKey: Joi.string().min(1).max(1024),
    salt: Joi.string().min(1).max(1024),
    socketId: Joi.string().trim(),
    chats: Joi.array().items(Joi.objectId()),
    contacts: Joi.array().items(Joi.object().keys({ user: Joi.objectId() })),
    blockedUsers: Joi.array().items(Joi.objectId()),
    defaultMsgDestruction: Joi.string().trim().lowercase().valid(...Object.values(expirations)).default(expirations.DEFAULT),
    decryptMsgByDefault: Joi.boolean().default(true),
    newMsgNotificationOn: Joi.boolean().default(true),
    createdAt: Joi.date().default(Date.now)
  });

  return schema.validate(user);
}

function validateLogin(login) {
  const schema = Joi.object({
    username: Joi.string().trim().min(4).max(50).required(),
    password: Joi.string().min(7).max(1024).required().strict()
  });

  return schema.validate(login);
}

function validateCrypto(cryptoData) {
  const schema = Joi.object({
    privKey: Joi.string().min(1).max(3000),
    pubKey: Joi.string().min(1).max(1024),
    salt: Joi.string().min(1).max(1024)
  });

  return schema.validate(cryptoData);
}

module.exports = {
  User,
  validateUser: validate,
  validateLogin,
  validateCrypto,
  encryptPW
};
