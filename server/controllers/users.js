const { User } = require('../models/user');
const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

exports.getFromUsername = async (req, res, next) => {
  let user = await User.findOne({ username: req.params.username.toLowerCase() });

  if (!user)
    return next({
      status: 404,
      message: 'There is no user with the given username.'
    });

  res.send({ data: populateUserResponse(user) });
};

exports.getFromId = async (req, res, next) => {
  let user = await User.findById(req.params.id);

  if (!user)
    return next({
      status: 404,
      message: 'There is no user with the given ID.'
    });

  res.send({ data: populateUserResponse(user) });
};

exports.updateCryptoData = async (req, res, next) => {
  const updatedUser = await User.findByIdAndUpdate(req.params.id, {
    pubKey: req.body.pubKey,
    privKey: req.body.privKey,
    salt: req.body.salt
  }, { new: true });

  if (!updatedUser)
    return next({
      status: 404,
      message: 'There is no user with the given ID.'
    });

  res.send({ data: populateUserResponse(updatedUser) });
};

const populateUserResponse = (user) => ({
  _id: user._id,
  username: user.username,
  socketId: user.socketId,
  pubKey: user.pubKey
});
