require('dotenv').config();
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const isValidObjectId = mongoose.Types.ObjectId.isValid;
const { User } = require('../models/user');
const { Chat, validateChat } = require('../models/chat');
const { Message, validateMsg } = require('../models/message');
const { getExpirationDate, expirations } = require('../utils/msgExpirations');

exports.updateUserSocketId = async ({ userId, socketId }) => {
  if (!isValidObjectId(userId))
    return null;

  if (!isString(socketId))
    return null;

  await User.findByIdAndUpdate(userId, { socketId });
};

exports.resetUserSocketId = async (userId) => {
  if (!isValidObjectId(userId))
    return null;

  await User.findByIdAndUpdate(userId, { socketId: '' });
};

exports.loadSettings = async (userId) => {
  if (!isValidObjectId(userId))
    return null;

  const user = await User.findById(userId);

  if (!user)
    return null;

  const settings = {
    defaultMsgDestruction: user.defaultMsgDestruction,
    decryptMsgByDefault: user.decryptMsgByDefault,
    newMsgNotificationOn: user.newMsgNotificationOn,
    createdAt: user.createdAt
  };

  return settings;
};

exports.createChat = async ({ senderId, receiverId }) => {
  if (!isValidObjectId(senderId) || !isValidObjectId(receiverId))
    return null;

  const sender = await User.findById(senderId);
  const receiver = await User.findById(receiverId);

  if (!sender || !receiver)
    return null;

  // check if a chat between the sender and receiver already exists
  let chat = await Chat.findOne({
    $and: [
      { members: { $elemMatch: { user: sender._id } } },
      { members: { $elemMatch: { user: receiver._id } } }
    ]
  });

  if (chat) {
    const senderHasTheChat = sender.chats.some(c => idsMatch(c, chat._id));
    if (!senderHasTheChat) {
      sender.chats.push(chat);
      await sender.save();
    }

    return chat;
  }

  // create a new chat if it does not exist
  chat = {
    members: [
      {
        user: sender._id.toString(),
        msgDestructionOpt: sender.defaultMsgDestruction
      },
      {
        user: receiver._id.toString(),
        msgDestructionOpt: receiver.defaultMsgDestruction
      }
    ]
  };

  const { error } = validateChat(chat);

  if (error)
    return null;

  chat = await Chat.create(chat);
  sender.chats.push(chat);
  await sender.save();

  return chat;
};

exports.loadChats = async (userId) => {
  if (!isValidObjectId(userId))
    return null;

  const user = await User.findById(userId);

  if (!user)
    return null;

  // some chats that have previously been deleted
  // might still exist as references in the user's
  // chat list. Chat.find will return only the valid ones
  const chats = await Chat.find({ _id: { $in: user.chats } });

  if (!chats)
    return [];

  // if the user's chat list and the actual chats that
  // exist are not in sync, update the user's chat list
  if (areOutOfSync(chats, user.chats)) {
    user.chats = chats;
    await user.save();
  }

  return chats;
};

exports.loadUnreadMsgs = async ({ userId, chats }) => {
  if (!isValidObjectId(userId))
    return null;

  const user = await User.findById(userId);

  if (!user || !Array.isArray(chats) || !chats.length)
    return null;

  const unreadMsgs = [];

  for (let chat of chats) {
    const msgs = await Message.find({ _id: { $in: chat.messages } });
    const unreadMsgIds = [];

    for (let msg of msgs) {
      const userIsReceiver = idsMatch(msg.receiver._id, userId);

      if (userIsReceiver && !msg.hasBeenRead)
        unreadMsgIds.push(msg._id);
    }

    if (unreadMsgIds.length)
      unreadMsgs.push({ chatId: chat._id, unreadMsgIds });
  }

  return unreadMsgs;
};

exports.markAsRead = async ({ unreadMsgIds, userId }) => {
  if (!Array.isArray(unreadMsgIds) || !unreadMsgIds.length)
    return null;

  const ids = [ ...unreadMsgIds, userId ];
  const invalidIdExists = ids.some(id => !isValidObjectId(id));
  if (invalidIdExists)
    return null;

  let msgs = await Message.find({ _id: { $in: unreadMsgIds }});

  if (!msgs || !msgs.length)
    return null;

  msgs = msgs.filter(m => idsMatch(m.receiver, userId));

  if (!msgs.length)
    return null;

  await Message.updateMany(
    { _id: { $in: msgs } },
    { $set: { 'hasBeenRead': true } }
  );

  /* or:
  await Message.updateMany(
    {$and: [
      { _id: { $in: unreadMsgIds }},
      { receiver: userId }
    ]},
    { $set: { 'hasBeenRead': true } }
  );
  */

  return true;
};

exports.setMsgDestruction = async ({ chatId, userId, destructionOpt }) => {
  if (!isValidObjectId(userId) || !isValidObjectId(chatId))
    return null;

  const user = await User.findById(userId);
  const chat = await Chat.findById(chatId);

  if (!user || !chat)
    return null;

  const isValidDestructionOpt = Object.values(expirations).includes(destructionOpt);
  if (!isValidDestructionOpt)
    destructionOpt = expirations.DEFAULT;

  await Chat.updateOne(
    { _id: chatId },
    { $set: { 'members.$[elem].msgDestructionOpt': destructionOpt } },
    { arrayFilters: [ { 'elem.user': user._id } ] }
  );

  return true;
};

exports.createMsg = async (data) => {
  const { chatId, sender, receiver, msgEncryptedForSender, msgEncryptedForReceiver } = data;

  if (!chatId || !sender || !sender._id || !receiver || !receiver._id)
    return null;

  if (!isString(msgEncryptedForSender) || !isString(msgEncryptedForReceiver))
    return null;

  if (!msgEncryptedForSender || !msgEncryptedForReceiver)
    return null;

  const ids = [ chatId, sender._id, receiver._id ];
  const invalidIdExists = ids.some(id => !isValidObjectId(id));
  if (invalidIdExists)
    return null;

  const chat = await Chat.findById(chatId);
  const senderDb = await User.findById(sender._id);
  const receiverDb = await User.findById(receiver._id);

  if (!chat || !senderDb || !receiverDb)
    return null;

  const expiresIn = chat.members
    .find(m => idsMatch(m.user, sender._id))
    .msgDestructionOpt;

  let msg = {
    sender: sender._id.toString(),
    receiver: receiver._id.toString(),
    msgEncryptedForSender,
    msgEncryptedForReceiver,
    expireAt: getExpirationDate(expiresIn)
  };

  const { error } = validateMsg(msg);

  if (error)
    return null;

  msg = await Message.create(msg);
  chat.messages.push(msg);
  chat.mostRecentMsgDate = msg.createdAt;
  await chat.save();

  // if the receiver does not have the chat in its
  // chat list, add it
  const receiverHasTheChat = receiverDb.chats.some(c => idsMatch(c._id, chat._id));
  if (!receiverHasTheChat) {
    receiverDb.chats.push(chat);
    await receiverDb.save();
  }

  return { msg, chat };
};

exports.loadChatMsgs = async (chatId) => {
  if (!isValidObjectId(chatId))
    return null;

  const chat = await Chat.findById(chatId);

  if (!chat)
    return null;

  // some messages that have previously been deleted
  // might still exist as references in the user's chat
  // messages list. Message.find will return only the valid ones
  const msgs = await Message.find({ _id: { $in: chat.messages } });

  if (!msgs)
    return [];

  // if the user's chat messages list and the actual messages
  // exist are not in sync, update the user's chat messages list
  if (areOutOfSync(msgs, chat.messages)) {
    chat.messages = msgs;
    await chat.save();
  }

  return msgs;
};

exports.getCryptoData = async (userId) => {
  if (!isValidObjectId(userId))
    return null;

  const user = await User.findById(userId);

  if (!user)
    return null;

  return {
    privKey: user.privKey,
    salt: user.salt
  };
};

exports.storeCryptoData = async ({ userId, pubKey, encrPrivKey, salt }) => {
  if (!isValidObjectId(userId))
    return null;

  await User.findByIdAndUpdate(userId, { pubKey, privKey: encrPrivKey, salt });
};

exports.loadContacts = async (userId) => {
  if (!isValidObjectId(userId))
    return null;

  const user = await User.findById(userId);
  if (!user)
    return null;

  const contacts = [];

  for (let c of user.contacts) {
    const contact = await User.findById(c.user).select('username blockedUsers'); // '_id' is automatically selected

    if (contact) {
      contacts.push({
        _id: c.user,
        username: contact.username,
        blocked: user.blockedUsers.some(bu => idsMatch(bu, c.user)),
        blockedByContact: contact.blockedUsers.some(bu => idsMatch(bu, userId)),
      });
    }
    // if contact does not exist in DB, the contact may have
    // deleted its account. Thus, remove the contact from the
    // user's contact list
    else {
      await User.updateOne(
        { _id: userId },
        { $pull: { contacts: { user: c.user } } },
        { safe: true }
      );
    }
  }

  return contacts;
};

exports.storeContact = async ({ userId, contactId }) => {
  if (!isValidObjectId(userId) || !isValidObjectId(contactId))
    return null;

  const user = await User.findById(userId);
  const contact = await User.findById(contactId);

  if (!user || !contact)
    return null;

  const contactAlreadyExists = user.contacts.some(c => idsMatch(c.user, contactId));

  if (contactAlreadyExists)
    return null;

  user.contacts.push({ user: contactId });
  await user.save();

  return true;
};

exports.loadSearchRes = async ({ usernamePrefix, searcherId }) => {
  if (!isValidObjectId(searcherId))
    return null;

  if (!isString(usernamePrefix))
    return null;

  const MIN_SEARCH_LENGTH = 3;
  const res = [];

  if (!usernamePrefix || usernamePrefix.length < MIN_SEARCH_LENGTH)
    return res;

  const users = await User
              //.find({ $text: { $search: usernamePrefix, $caseSensitive: false }})
              .find({ username: new RegExp(usernamePrefix, 'gi')})
              .sort('username')
              .limit(30)
              .select('username blockedUsers'); // '_id' is automatically selected;

  const searcher = await User.findById(searcherId).select('blockedUsers');

  for (let user of users) {
    // skip if the result is the searcher itself
    if (idsMatch(user._id, searcherId))
      continue;

    res.push({
      _id: user._id,
      username: user.username,
      blocked: searcher.blockedUsers.some(bu => idsMatch(bu, user._id)),
      blockedByContact: user.blockedUsers.some(bu => idsMatch(bu, searcherId))
    });
  }

  return res;
};

exports.blockContact = async ({ userId, contactId }) => {
  if (!isValidObjectId(userId) || !isValidObjectId(contactId))
    return null;

  const user = await User.findById(userId);
  const contact = await User.findById(contactId);

  if (!user || !contact)
    return null;

  const contactIsAlreadyBlocked = user.blockedUsers.some(bu => idsMatch(bu, contactId));
  if (contactIsAlreadyBlocked)
    return null;

  user.blockedUsers.push(contactId);
  await user.save();

  return { contactSocketId: contact.socketId };
};

exports.unblockContact = async ({ userId, contactId }) => {
  if (!isValidObjectId(userId) || !isValidObjectId(contactId))
    return null;

  const user = await User.findById(userId);
  const contact = await User.findById(contactId);

  if (!user || !contact)
    return null;

  const contactIsBlocked = user.blockedUsers.some(bu => idsMatch(bu, contactId));
  if (!contactIsBlocked)
    return null;

  await User.updateOne(
    { _id: userId },
    { $pull: { blockedUsers: contactId } },
    { safe: true }
  );

  return { contactSocketId: contact.socketId };
};

exports.deleteContact = async ({ userId, contactId }) => {
  if (!isValidObjectId(userId) || !isValidObjectId(contactId))
    return null;

  const user = await User.findById(userId);

  // not checking if the contact exists because the contact might
  // have deleted its account while the user's reference to the
  // contact still exists
  if (!user)
    return null;

  // remove the contact from the user's contact list
  await User.updateOne(
    { _id: userId },
    { $pull: { contacts: { user: contactId } } },
    { safe: true }
  );

  // also delete their chat and messages if needed:
  const chat = await Chat.findOne({
    $and: [
      { members: { $elemMatch: { user: userId } } },
      { members: { $elemMatch: { user: contactId } } }
    ]
  });

  if (chat)
    await this.deleteChat({ userId, chatId: chat._id });

  return true;
};

exports.deleteChat = async ({ userId, chatId }) => {
  if (!isValidObjectId(userId) || !isValidObjectId(chatId))
    return null;

  const user = await User.findById(userId);
  const chat = await Chat.findById(chatId);

  if (!user || !chat)
    return null;

  // remove the chat reference from the user's chat list
  await User.updateOne(
    { _id: userId },
    { $pull: { chats: chatId } },
    { safe: true }
  );

  // retrieve the partner's chats and search for the chat
  const partnerId = chat.members.filter(m => !idsMatch(m.user, userId))[0].user;
  const partner = await User.findById(partnerId);
  const partnerHasTheChat = partner && partner.chats.some(c => idsMatch(c, chatId));

  // if the chat partner does not have the chat in its list,
  // delete the chat and its messages permanently
  if (!partnerHasTheChat) {
    await Message.deleteMany({ _id: { $in: chat.messages } });
    await Chat.deleteOne({ _id: chatId });
  }

  return true;
};

exports.updateDefaultMsgDestruction = async ({ userId, defaultMsgDestruction }) => {
  if (!isValidObjectId(userId))
    return null;

  const isValidDestructionOpt = Object.values(expirations).includes(defaultMsgDestruction);
  if (!isValidDestructionOpt)
    defaultMsgDestruction = expirations.DEFAULT;

  await User.findByIdAndUpdate(userId, { defaultMsgDestruction });
};

exports.updateDecryptMsgByDefault = async ({ userId, decryptMsgByDefault }) => {
  if (!isValidObjectId(userId))
    return null;
  
  if (!isBoolean(decryptMsgByDefault))
    return null;

  await User.findByIdAndUpdate(userId, { decryptMsgByDefault });
};

exports.updateNewMsgNotification = async ({ userId, newMsgNotificationOn }) => {
  if (!isValidObjectId(userId))
    return null;
  
  if (!isBoolean(newMsgNotificationOn))
    return null;
  
  await User.findByIdAndUpdate(userId, { newMsgNotificationOn });
};

exports.deleteAccount = async ({ userId, password, token}) => {
  const error = { error: { message: 'Could not authenticate user.' } };

  if (!isValidObjectId(userId))
    return error;

  if (!password || !token)
    return error;

  const user = await User.findById(userId);
  if (!user)
    return error;

  const isCorrectPassword = await user.comparePW(password);
  if (!isCorrectPassword)
    return error;

  // verify the authentication token
  try {
    jwt.verify(token, process.env.JWT_SECRET_KEY);
  }
  catch (err) {
    return error;
  }

  for (let chat of user.chats)
    await this.deleteChat({ userId, chatId: chat });

  await User.deleteOne({ _id: userId });

  return { success: true };
};

const areOutOfSync = (expected, actual) => {
  return (expected.length !== actual.length) ||
        (expected.some(elem => !actual.includes(elem._id)));
};

const idsMatch = (id1, id2) => id1.toString() === id2.toString();

const isBoolean = (val) => typeof val === 'boolean';

const isString = (val) => typeof val === 'string';
