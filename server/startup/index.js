module.exports = function(app, io) {
  require('./logging')(app);
  require('./socket')(io);
  require('./routes')(app);
  require('./db')();
  require('./prod')(app);
};
