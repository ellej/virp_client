const socketManager = require('../socket/socketManager');

module.exports = function(io) {
  socketManager(io);
};
