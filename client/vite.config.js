import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  // Adding `resolve` to fix type error related to `react-moment`.
  // (See: https://github.com/vitejs/vite/issues/7376#issuecomment-1469998894)
  resolve: {
    mainFields: [],
  },
});
