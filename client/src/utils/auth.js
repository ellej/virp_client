import jwtDecode from 'jwt-decode';
import { LOCAL_STORAGE_TOKEN_NAME, getItem, setItem, removeItem } from './localStorage';

export const getToken = () => getItem(LOCAL_STORAGE_TOKEN_NAME);

export const setToken = (token) => setItem(LOCAL_STORAGE_TOKEN_NAME, token);

export const removeToken = () => removeItem(LOCAL_STORAGE_TOKEN_NAME);

export const decodeToken = (token) => {
  try {
    return jwtDecode(token);
  }
  catch (err) {
    return { error: err };
  }
};

export const getTokenExpirationMilliseconds = () => {
  const { error, exp } = decodeToken(getToken());

  if (error)
    return 0;

  const millisecondsLeft = exp * 1000 - Date.now();

  return millisecondsLeft < 0 ? 0 : millisecondsLeft;
}

export const tokenHasExpired = () => {
  const { error, exp } = decodeToken(getToken());

  return !!error || Date.now() >= exp * 1000;
};
