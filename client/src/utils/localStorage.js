export const LOCAL_STORAGE_TOKEN_NAME = 'jwtToken';

export const getItem = (name) => JSON.parse(localStorage.getItem(name)) || '';

export const setItem = (name, value) => localStorage.setItem(name, JSON.stringify(value));

export const removeItem = (name) => localStorage.removeItem(name);
