// Extra extra small devices (portrait phones, less than 375px)
// @media (max-width: 375px) { ... }

// Extra small devices (portrait phones, less than 600px)
// @media (max-width: 600px) { ... }

// Small devices (landscape phones, less than 768px)
// @media (max-width: 768px) { ... }

// Medium devices (tablets, less than 992px)
// @media (max-width: 992px) { ... }

// Large devices (desktops, less than 1200px)
// @media (max-width: 1200px) { ... }

// Extra large devices (large desktops, less than 1600px)
// @media (max-width: 1600px) { ... }

const getMediaQueryMaxWidth = (width) => `@media (max-width: ${width}px)`;
const getMediaQueryMaxHeight = (height) => `@media (max-height: ${height}px)`;
const getMediaQueryMaxHeightLaptop = (height) => `@media screen and (min-width: 1024px) and (max-height: ${height}px)`;

const maxWidth = {
  xxs: () => getMediaQueryMaxWidth(375),
  xs: () => getMediaQueryMaxWidth(600),
  sm: () => getMediaQueryMaxWidth(768),
  md: () => getMediaQueryMaxWidth(992),
  lg: () => getMediaQueryMaxWidth(1200),
  xl: () => getMediaQueryMaxWidth(1400),
  custom: (width) => getMediaQueryMaxWidth(width)
};

const maxHeight = {
  xxs: () => getMediaQueryMaxHeight(375),
  xs: () => getMediaQueryMaxHeight(414),
  sm: () => getMediaQueryMaxHeight(520),
  md: () => getMediaQueryMaxHeight(660),
  lg: () => getMediaQueryMaxHeight(768),
  xl: () => getMediaQueryMaxHeight(850),
  xxl: () => getMediaQueryMaxHeight(1024),
  custom: (height) => getMediaQueryMaxHeight(height)
};

const maxHeightLaptop = {
  md: () => getMediaQueryMaxHeightLaptop(660),
  lg: () => getMediaQueryMaxHeightLaptop(768),
  xl: () => getMediaQueryMaxHeightLaptop(850),
  xxl: () => getMediaQueryMaxHeightLaptop(1024),
  custom: (height) => getMediaQueryMaxHeightLaptop(height)
};

export default { maxWidth, maxHeight, maxHeightLaptop };
