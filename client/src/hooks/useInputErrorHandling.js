import { useState } from 'react';

function useInputErrorHandling(defaultError) {
  const [ inputError, setInputError ] = useState(defaultError);

  const addInputError = (type, message) => setInputError({ type, message });

  const removeInputError = () => setInputError(null);

  return { inputError, addInputError, removeInputError };
}

export default useInputErrorHandling;
