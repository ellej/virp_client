import { ADD_UNREAD_MSGS, ADD_UNREAD_MSG, MARK_AS_READ } from './actionTypes';

export const addUnreadMsgs = (unreadMsgs, dispatch) => {
  dispatch({
    type: ADD_UNREAD_MSGS,
    unreadMsgs
  });
};

export const addUnreadMsg = (unreadMsgId, chatId, dispatch) => {
  dispatch({
    type: ADD_UNREAD_MSG,
    unreadMsgId,
    chatId
  });
};

export const markAsRead = (chatId, dispatch) => {
  dispatch({
    type: MARK_AS_READ,
    chatId
  });
};
