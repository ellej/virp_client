import { ADD_TOAST, REMOVE_TOAST } from './actionTypes';

export const addToast = (toastId, toastType, toastMsg, dispatch) => {
  dispatch({
    type: ADD_TOAST,
    toastId,
    toastType,
    toastMsg
  });

  const TOAST_DURATION = 4500;
  setTimeout(() => removeToast(toastId, dispatch), TOAST_DURATION);
};

export const removeToast = (toastId, dispatch) => {
  dispatch({
    type: REMOVE_TOAST,
    toastId
  });
};
