import { setAuth, removeAuth } from '../services/auth';
import { USER_DISCONNECTED } from '../utils/socket';
import { SET_CURRENT_USER } from './actionTypes';

export const setCurrentUser = (user, dispatch) => {
  // the server populates the user object with
  // an _id, username, pubKey, and token field
  const { _id, username, pubKey, token } = user;

  setAuth(token);

  dispatch({
    type: SET_CURRENT_USER,
    user: {
      _id,
      username,
      pubKey,
      isAuthenticated: true
    }
  });
};

export const logout = (dispatch, emit) => {
  removeAuth();
  emit(USER_DISCONNECTED);

  dispatch({
    type: SET_CURRENT_USER,
    user: {}
  });
};
