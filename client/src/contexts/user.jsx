import { useReducer, createContext } from 'react';
import reducer from '../reducers/user';
import { setAuth } from '../services/auth';
import { getToken, decodeToken } from '../utils/auth';

export const UserContext = createContext();
export const UserDispatchContext = createContext();

const defaultUser = {};

function init() {
  const token = getToken();

  if (!token)
    return defaultUser;
  
  const { error, _id, username, pubKey } = decodeToken(token);

  if (error) {
    console.warn('Error when decoding token: ', error);
    return defaultUser;
  }

  setAuth(token);

  return {
    _id,
    username,
    pubKey,
    isAuthenticated: true
  };
}

export function UserProvider(props) {
  const [ user, dispatch ] = useReducer(reducer, defaultUser, init);

  return (
    <UserContext.Provider value={user}>
      <UserDispatchContext.Provider value={dispatch}>
        {props.children}
      </UserDispatchContext.Provider>
    </UserContext.Provider>
  );
}
