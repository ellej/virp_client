import { useReducer, createContext } from 'react';
import { chatsReducer, openedChatReducer } from '../reducers/chats';

export const ChatsContext = createContext();
export const ChatsDispatchContext = createContext();
export const OpenedChatContext = createContext();
export const OpenedChatDispatchContext = createContext();

const defaultChats = [];
const defaultOpenedChat = '';

export function ChatsProvider(props) {
  const [ chats, chatsDispatch ] = useReducer(chatsReducer, defaultChats);
  const [ openedChat, openedChatDispatch ] = useReducer(openedChatReducer, defaultOpenedChat);

  return (
    <ChatsContext.Provider value={chats}>
      <ChatsDispatchContext.Provider value={chatsDispatch}>
        <OpenedChatContext.Provider value={openedChat}>
          <OpenedChatDispatchContext.Provider value={openedChatDispatch}>
            {props.children}
          </OpenedChatDispatchContext.Provider>
        </OpenedChatContext.Provider>
      </ChatsDispatchContext.Provider>
    </ChatsContext.Provider>
  );
}
