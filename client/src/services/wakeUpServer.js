import http from './http';
import { API_URL, API_WAKE_UP_ENDPOINT } from '../config/urls';

export const wakeUpServer = () => {
  const apiEndpoint = API_URL + API_WAKE_UP_ENDPOINT;

  return http('GET', apiEndpoint);
};
