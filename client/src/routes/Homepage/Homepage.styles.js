import commonStyles from '../../utils/commonStyles';

const styles = {
  Homepage: {
    ...commonStyles.fullViewFlexColumn,
    background: commonStyles.backgroundLock,
  },
  content: commonStyles.fullWidthContentCentered,
  buttons: {
    display: 'flex'
  },
  button: commonStyles.btnLarge
};

export default styles;
