import { useState, useEffect, useContext } from 'react';
import { createUseStyles } from 'react-jss';
import { motion } from 'framer-motion';
import { UserDispatchContext } from '../../contexts/user';
import { SocketEmitContext } from '../../contexts/socket';
import { CryptoContext } from '../../contexts/crypto';
import { OpenedChatContext, OpenedChatDispatchContext } from '../../contexts/chats';
import { setOpenedChat } from '../../actions/chats';
import { logout } from '../../actions/auth';
import Sidebar from '../../components/Sidebar/Sidebar';
import MainWindow from '../../components/MainWindow/MainWindow';
import Loading from '../../components/Loading/Loading';
import Toasts from '../../components/Toasts/Toasts';
import { GET_CRYPTO_DATA, LOAD_SETTINGS, LOAD_CONTACTS } from '../../utils/socket';
import { getTokenExpirationMilliseconds } from '../../utils/auth';
import { TOAST_POS_BOTTOM } from '../../utils/toastPositions';
import commonStyles from '../../utils/commonStyles';
import styles from './Dashboard.styles';

const useStyles = createUseStyles(styles);

function Dashboard() {
  const [ isLoading, setIsLoading ] = useState(true);
  const [ openedBox, setOpenedBox ] = useState('');
  const userDispatch = useContext(UserDispatchContext);
  const openedChat = useContext(OpenedChatContext);
  const openedChatDispatch = useContext(OpenedChatDispatchContext);
  const cryptoData = useContext(CryptoContext);
  const emit = useContext(SocketEmitContext);

  useEffect(() => {
    const loadingTimerId = setTimeout(() => setIsLoading(false), 3000);
    const tokenExpiration = getTokenExpirationMilliseconds();
    const logoutTimerId = setTimeout(() => logout(userDispatch, emit), tokenExpiration);

    emit(LOAD_SETTINGS);
    emit(LOAD_CONTACTS);

    return () => {
      clearTimeout(loadingTimerId);
      clearTimeout(logoutTimerId);
    }
  }, []);

  useEffect(() => {
    if (!cryptoData)
      emit(GET_CRYPTO_DATA);
  }, [cryptoData]);

  useEffect(() => {
    if (openedChat && openedBox)
      closeOpenedBox();
  }, [openedChat]);

  const closeOpenedChat = () => setOpenedChat('', openedChatDispatch);
  const closeOpenedBox = () => setOpenedBox('');
  const switchOpenedBox = (newBox) => {
    if (openedChat)
      closeOpenedChat();

    if (newBox !== openedBox)
      setOpenedBox(newBox);
  };

  const classes = useStyles();

  return (
    <motion.div
      initial={commonStyles.pageTransitionVar.initial}
      animate={commonStyles.pageTransitionVar.animate}
      exit={commonStyles.pageTransitionVar.exit}
      variants={commonStyles.pageTransition.downToUp}
      className={classes.Dashboard}
    >
      {isLoading && (
        <div className={classes.loading}>
          <Loading
            type="Bars"
            color={commonStyles.brandGoldMain}
            height={170}
            width={170}
            timeout={3000}
          />
        </div>
      )}
      <Sidebar
        switchOpenedBox={switchOpenedBox}
        openedBox={openedBox}
      />
      <MainWindow
        openedChat={openedChat}
        openedBox={openedBox}
        switchOpenedBox={switchOpenedBox}
        closeOpenedBox={closeOpenedBox}
      />
      <Toasts position={TOAST_POS_BOTTOM} />
    </motion.div>
  );
}

export default Dashboard;
