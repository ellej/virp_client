import { useState, useContext } from 'react';
import { useHistory, Link } from 'react-router-dom';
import { v4 as uuidv4 } from 'uuid';
import { createUseStyles } from 'react-jss';
import { motion } from 'framer-motion';
import { UserDispatchContext } from '../../contexts/user';
import { CryptoSetContext } from '../../contexts/crypto';
import { ToastsDispatchContext } from '../../contexts/toasts';
import useCryptography from '../../hooks/useCryptography';
import useInput from '../../hooks/useInput';
import useInputErrorHandling from '../../hooks/useInputErrorHandling';
import { setCurrentUser } from '../../actions/auth';
import { addToast } from '../../actions/toasts';
import Header from '../../components/Header/Header';
import Toasts from '../../components/Toasts/Toasts';
import { loginOrSignup, setTokenHeader } from '../../services/auth';
import { updateUserById } from '../../services/user';
import { API_SIGNUP_ENDPOINT, API_LOGIN_ENDPOINT } from '../../config/urls';
import { ROUTE_HOME, ROUTE_LOGIN, ROUTE_SIGNUP, ROUTE_DASHBOARD } from '../../utils/routePaths';
import { INPUT_ERROR_PASSWORD, INPUT_ERROR_PASSWORD_CONF } from '../../utils/errorTypes';
import { TOAST_ERROR, TOAST_INFO } from '../../utils/toastTypes';
import { TOAST_POS_RIGHT } from '../../utils/toastPositions';
import commonStyles from '../../utils/commonStyles';
import styles from './AuthForm.styles';

const useStyles = createUseStyles(styles);

function AuthForm({ isNewUser, hasAttemptedServerAuthentication, setHasAttemptedServerAuthentication }) {
  const userDispatch = useContext(UserDispatchContext);
  const setCryptoData = useContext(CryptoSetContext);
  const toastsDispatch = useContext(ToastsDispatchContext);
  const history = useHistory();
  const [ forgotPassword, setForgotPassword ] = useState(false);
  const [ username, handleUsernameChange, resetUsername ] = useInput('');
  const [ password, handlePasswordChange, resetPassword ] = useInput('');
  const [ passwordConf, handlePasswordConfChange, resetPasswordConf ] = useInput('');
  const { inputError, addInputError, removeInputError } = useInputErrorHandling(null);
  const {
    initCrypto,
    generateSymKey,
    symDecrypt
  } = useCryptography('');

  /* --------- start: used for detecting spam: --------- */
  const [ email, handleEmailChange, resetEmail ] = useInput('');
  const [ previousSubmitTime, setPreviousSubmitTime ] = useState(null);
  /* -------------------- end -------------------- */

  const endpoint = isNewUser ? API_SIGNUP_ENDPOINT : API_LOGIN_ENDPOINT;

  const generateCryptoData = async ({ userId, token }) => {
    // generate key-pairs and other crypto data
    const { pubKey, privKey, encrPrivKey, salt } = initCrypto(password);

    setTokenHeader(token);
    const { error } = await updateUserById(userId, {
      pubKey,
      privKey: encrPrivKey,
      salt
    });

    if (error) {
      addToast(uuidv4(), TOAST_ERROR, error.message, toastsDispatch);
      console.warn('Error when updating user: ', error);
      return false;
    }

    setCryptoData({ privKey, salt, privKeyIsDecrypted: true }); // add pubkey too?

    return { pubKey };
  };

  const updateCryptoData = ({ encrPrivKey, salt }) => {
    const symKey = generateSymKey(password, salt);
    const privKey = symDecrypt(encrPrivKey, symKey);
    const { error } = privKey;
    if (error) {
      addToast(uuidv4(), TOAST_ERROR, 'Oops.. something went wrong.', toastsDispatch);
      console.warn('Error when decrypting private key: ', error);
      return false;
    }

    setCryptoData({ privKey, salt, privKeyIsDecrypted: true });

    return true;
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    /* --------- start: spam detection: --------- */
    const timeSubmitted = Date.now();

    if (spamDetected(timeSubmitted))
      return setPreviousSubmitTime(timeSubmitted);

    setPreviousSubmitTime(timeSubmitted);
    /* -------------------- end -------------------- */

    if (inputError)
      removeInputError();

    if (isNewUser && !isValidSignup())
      return;
    
    if (!isNewUser && !isValidLogin())
      return;

    if (!hasAttemptedServerAuthentication) {
      addToast(uuidv4(), TOAST_INFO, 'It may take a few seconds if the server was sleeping 😴..', toastsDispatch);
    }
    const credentials = { username, password };
    const { error, data } = await loginOrSignup(endpoint, credentials);
    setHasAttemptedServerAuthentication(true);

    if (error)
      return addToast(uuidv4(), TOAST_ERROR, error.message, toastsDispatch);

    if (keysExist(data.pubKey, data.encrPrivKey)) {
      const success = updateCryptoData({ encrPrivKey: data.encrPrivKey, salt: data.salt });
      if (!success)
        return;
    }
    else {
      const { pubKey } = await generateCryptoData({ userId: data._id, token: data.token });
      if (pubKey)
        data['pubKey'] = pubKey;
      else
        return;
    }

    setCurrentUser(data, userDispatch);
    history.push(ROUTE_DASHBOARD);
  };

  const keysExist = (pubKey, privKey) => (
    // I have to specifically check for 'undefined'
    pubKey !== undefined && privKey !== undefined
  );

  const isValidLogin = () => {
    if (isValidUsername() && isValidPassword())
      return true;

    addToast(uuidv4(), TOAST_ERROR, 'Invalid username or password.', toastsDispatch);
    return false;
  };

  const isValidSignup = () => {
    if (!isValidPassword()) {
      addInputError(INPUT_ERROR_PASSWORD, 'Password must be at least 7 characters!');
      return false;
    }

    if (!passwordsMatch()) {
      addInputError(INPUT_ERROR_PASSWORD_CONF, 'Passwords do not match!');
      return false;
    }

    return true;
  };

  const isValidUsername = () => username.length >= 4;

  const isValidPassword = () => password.length >= 7;
  
  const passwordsMatch = () => password === passwordConf;

  const resetForm = () => {
    resetUsername();
    resetPassword();
    resetPasswordConf();
    removeInputError();
  };

  const spamDetected = (timeSubmitted) => {
    const SUBMISSION_TIME_LIMIT = 1000;
    const submittedTooFast = previousSubmitTime && timeSubmitted - previousSubmitTime < SUBMISSION_TIME_LIMIT;

    return !!email || submittedTooFast;
  };

  const classes = useStyles();

  return (
    <div className={classes.AuthForm}>
      <Header />
      <motion.div
        initial={commonStyles.pageTransitionVar.initial}
        animate={commonStyles.pageTransitionVar.animate}
        exit={commonStyles.pageTransitionVar.exit}
        variants={commonStyles.pageTransition.fade}
        className={classes.content}
      >
        {forgotPassword ? (
          <motion.div
            variants={commonStyles.pageTransition.fade}
            className={classes.infoBox}
          >
            <p className={classes.infoBoxTitle}>Forgot your password?</p>
            <p>
              Since security and anonymity are priorities of ours, we do not
              store user emails or phone numbers. Resetting your password is
              therefore not possible as we cannot confirm that the request is
              coming from the authorized source.
            </p>
            <p>
              In order to access Virp, you will need to register a new account.
              See you back soon! 
            </p>
          </motion.div>
        ) : (
          <>
          <form onSubmit={handleSubmit}>
            <div className={classes.formRow}>
              <span className={`${classes.icon} ${classes.iconUser}`} />
              <input
                type="text"
                autoComplete="off"
                id="username"
                placeholder="Enter username"
                value={username}
                onChange={handleUsernameChange}
                required
              />
            </div>
            <div className={classes.formRow}>
              <span className={`${classes.icon} ${classes.iconPassword}`} />
              <input
                type="password"
                autoComplete="off"
                id="password"
                placeholder={isNewUser ? 'Enter strong password' : 'Enter password'}
                value={password}
                onChange={handlePasswordChange}
                required
              />
              {(inputError && inputError.type === INPUT_ERROR_PASSWORD) && (
                <div className={classes.inputErrorMessage}>
                  {inputError.message}
                </div>
              )}
              {(inputError && inputError.type === INPUT_ERROR_PASSWORD_CONF) && (
                <div className={classes.inputErrorMessage}>
                  {inputError.message}
                </div>
              )}
            </div>
            {isNewUser && (
              <div className={classes.formRow}>
                <span className={`${classes.icon} ${classes.iconPassword}`} />
                <input
                  type="password"
                  autoComplete="off"
                  id="passwordConf"
                  placeholder="Confirm password"
                  value={passwordConf}
                  onChange={handlePasswordConfChange}
                  required
                />
                {(inputError && inputError.type === INPUT_ERROR_PASSWORD_CONF) && (
                  <div className={classes.inputErrorMessage}>
                    {inputError.message}
                  </div>
                )}
              </div>
            )}
            {/* --------- start: hidden field for detecting spam: --------- */}
            <input
              type="text"
              name="email"
              tabIndex={-1}
              autoComplete="off"
              value={email}
              onChange={handleEmailChange}
              className={classes.email}
            />
            {/* -------------------- end -------------------- */}
            <button type="submit">
              {isNewUser ? 'SIGN UP' : 'LOG IN'}
            </button>
          </form>
          <div className={classes.footer}>
            {isNewUser
              ? (
                <Link to={ROUTE_LOGIN}>
                  Already a member? Click here to log in.
                </Link>
              ) : (
                <>
                <span onClick={() => setForgotPassword(true)}>Forgot your password? |</span>
                <Link to={ROUTE_SIGNUP}> Need an account?</Link>
                </>
              )
            }
          </div>
          </>
        )}
        <div className={classes.backBtn}>
          <Link to={ROUTE_HOME}>&#171; back</Link>
        </div>
      </motion.div>
      <Toasts position={TOAST_POS_RIGHT} />
    </div>
  );
}

export default AuthForm;
