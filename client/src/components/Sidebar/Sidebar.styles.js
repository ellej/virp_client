import commonStyles from '../../utils/commonStyles';

const styles = {
  Sidebar: {
    width: commonStyles.sidebarWidth,
    minWidth: commonStyles.sidebarMinWidth,
    height: '100vh',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    background: 'linear-gradient(149deg, rgba(43,48,69,1) 0%, rgba(32,37,53,1) 100%)',
    boxShadow: '4px 0px 15px 0px rgba(23,23,23,1)',
    zIndex: '10'
  },
  headerAndChatsList: {
    height: `calc(100% - ${commonStyles.sidebarFooterHeight})`
  }
};

export default styles;
