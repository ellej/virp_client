import commonStyles from '../../../utils/commonStyles';

const styles = {
  ChatsList: {
    width: '100%',
    height: `calc(100% - ${commonStyles.sidebarHeaderHeight})`,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  subheading: {
    marginLeft: '20px',
    marginBottom: '15px',
    fontSize: '12px',
    fontWeight: '600',
    color: commonStyles.quietGray,
    alignSelf: 'flex-start'
  },
  chats: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    overflowY: 'auto',
    scrollbarWidth: 'none' // firefox
  }
};

export default styles;
