import { useContext } from 'react';
import { createUseStyles } from 'react-jss';
import { ChatsContext, OpenedChatContext } from '../../../contexts/chats';
import { UnreadMsgsContext } from '../../../contexts/unreadMsgs';
import ChatsListItem from './ChatsListItem/ChatsListItem';
import styles from './ChatsList.styles';

const useStyles = createUseStyles(styles);

function ChatsList({ searchVal, clearSearch }) {
  const chats = useContext(ChatsContext);
  const openedChat = useContext(OpenedChatContext);
  const unreadMsgs = useContext(UnreadMsgsContext);

  const getUnreadMsgIdsForChat = (chatId) => {
    const res = unreadMsgs.filter(elem => idsMatch(elem.chatId, chatId))[0];

    return res ? res.unreadMsgIds : [];
  };

  const idsMatch = (id1, id2) => id1.toString() === id2.toString();

  const classes = useStyles();

  return (
    <div className={classes.ChatsList}>
      <div className={classes.subheading}>
        ACTIVE CHATS
      </div>
      <div className={classes.chats}>
        {chats.map((chat, idx) => (
          <ChatsListItem
            key={chat._id}
            chat={chat}
            isOpen={openedChat && openedChat._id.toString() === chat._id.toString()}
            unreadMsgIds={getUnreadMsgIdsForChat(chat._id)}
            searchVal={searchVal}
            clearSearch={clearSearch}
          />
        ))}
      </div>
    </div>
  );
}

export default ChatsList;
