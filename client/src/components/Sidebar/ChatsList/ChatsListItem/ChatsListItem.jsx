import { useState, useEffect, useContext } from 'react';
import { createUseStyles } from 'react-jss';
import { motion } from 'framer-motion';
import Moment from 'react-moment';
import { UserContext } from '../../../../contexts/user';
import { OpenedChatDispatchContext } from '../../../../contexts/chats';
import { UnreadMsgsDispatchContext } from '../../../../contexts/unreadMsgs';
import { SocketEmitContext } from '../../../../contexts/socket';
import { setOpenedChat } from '../../../../actions/chats';
import { markAsRead } from '../../../../actions/unreadMsgs';
import { getChatPartner } from '../../../../services/user';
import { MARK_AS_READ } from '../../../../utils/socket';
import styles from './ChatsListItem.styles';

const useStyles = createUseStyles(styles);

function ChatsListItem({ chat, isOpen, unreadMsgIds, searchVal, clearSearch }) {
  const user = useContext(UserContext);
  const openedChatDispatch = useContext(OpenedChatDispatchContext);
  const unreadMsgsDispatch = useContext(UnreadMsgsDispatchContext);
  const emit = useContext(SocketEmitContext);
  const [ chatPartner, setChatPartner ] = useState('');
  const [ chatPartnerAccountDeleted, setChatPartnerAccountDeleted ] = useState(false);

  const hasUnreadMsgs = unreadMsgIds.length > 0;

  const getUnreadMsgsNum = () => {
    if (!hasUnreadMsgs)
      return <>&#10003;</>;
    
    if (unreadMsgIds.length > 9)
      return '9+';

    return unreadMsgIds.length;
  };

  const getAvatar = () => {
    if (chatPartnerAccountDeleted)
      return <>&#10005;</>;

    return chatPartner.username && chatPartner.username.charAt(0).toUpperCase();
  };

  const getChatPartnerUsername = () => {
    if (!chatPartner.username)
      return '';
    
    const UI_FRIENDLY_USERNAME_MAX_LENGTH = 17;

    return chatPartner.username.length < UI_FRIENDLY_USERNAME_MAX_LENGTH
      ? chatPartner.username
      : `${chatPartner.username.substring(0, UI_FRIENDLY_USERNAME_MAX_LENGTH)}...`;
  };

  useEffect(() => {
    const initChatPartner = async () => {
      // the server returns an object with either an error or a data property
      const { data, error } = await getChatPartner(chat, user);
      if (data) {
        setChatPartnerAccountDeleted(false);
        setChatPartner(data);
      }
      // handle the case where the chat partner does not exist
      // (the chat partner might have deleted its account)
      else if (error) {
        setChatPartnerAccountDeleted(true);
        setChatPartner({ _id: '', username: 'account deleted' });
      }
    }

    initChatPartner();
  }, [chat._id]);

  useEffect(() => {
    if (!isOpen)
      return;

    // mark messages as read when they come in if chat is opened
    markChatMessagesAsRead();
  }, [unreadMsgIds]);

  const openChat = () => {
    if (searchVal)
      clearSearch();

    if (isOpen)
      return;

    markChatMessagesAsRead();
    setOpenedChat(chat, openedChatDispatch);
  };

  const markChatMessagesAsRead = () => {
    if (hasUnreadMsgs) {
      markAsRead(chat._id, unreadMsgsDispatch);   // for local memory
      emit(MARK_AS_READ, unreadMsgIds);           // for db
    }
  };

  const calendarStrings = {
    lastDay : '[Yesterday at] LT',
    sameDay : '[Today at] LT',
    nextDay : '[Tomorrow at] LT',
    lastWeek : '[Last] dddd [at] LT',
    nextWeek : 'dddd [at] LT',
    sameElse : 'MMM DD[,] YYYY [at] LT'
  };

  const classes = useStyles({ hasUnreadMsgs, isOpen, chatPartnerAccountDeleted });

  // if the user is searching, only render the component if
  // the chat partner's username matches the search value
  if (searchVal) {
    const regex = new RegExp(searchVal, 'gi');
    if (!regex.test(chatPartner.username))
      return <></>
  }

  return (
    <motion.div
      positionTransition
      className={classes.ChatsListItem}
      onClick={openChat}
    >
      <div className={classes.infoBox}>
        <div className={classes.avatar}>
          <span>
            {getAvatar()}
          </span>
        </div>
        <div className={classes.infoContent}>
          <span className={classes.username}>{getChatPartnerUsername()}</span>
          {chat.mostRecentMsgDate ? (
            <Moment
              calendar={calendarStrings}
              className={classes.time}
            >
              {chat.mostRecentMsgDate}
            </Moment>
          ) : <span className={classes.time}>No messages</span>}
        </div>
      </div>
      <span className={classes.unreadMsgs}>{getUnreadMsgsNum()}</span>
    </motion.div>
  );
}

export default ChatsListItem;
