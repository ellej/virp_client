import { useContext } from 'react';
import { createUseStyles } from 'react-jss';
import { SocketEmitContext } from '../../../contexts/socket';
import { UserDispatchContext } from '../../../contexts/user';
import { logout } from '../../../actions/auth';
import { OPENED_BOX_CONTACTS, OPENED_BOX_SETTINGS, OPENED_BOX_ABOUT } from '../../../utils/openedBox';
import iconContactsGrey from '../../../assets/iconContactsGrey.png';
import iconContactsGold from '../../../assets/iconContactsGold.png';
import iconSettingsGrey from '../../../assets/iconSettingsGrey.png';
import iconSettingsGold from '../../../assets/iconSettingsGold.png';
import iconAboutGrey from '../../../assets/iconAboutGrey.png';
import iconAboutGold from '../../../assets/iconAboutGold.png';
import styles from './SidebarFooter.styles';

const useStyles = createUseStyles(styles);

function SidebarFooter({ switchOpenedBox, openedBox }) {
  const userDispatch = useContext(UserDispatchContext);
  const emit = useContext(SocketEmitContext);

  const classes = useStyles(openedBox);

  return (
    <div className={classes.SidebarFooter}>
      <div className={classes.footerTop}>
        <img
          className={`${classes.footerOpt} ${classes.footerOptContacts}`}
          src={openedBox === OPENED_BOX_CONTACTS ? iconContactsGold : iconContactsGrey}
          alt='contacts'
          onClick={() => switchOpenedBox(OPENED_BOX_CONTACTS)}  
        />
        <img
          className={`${classes.footerOpt} ${classes.footerOptSettings}`}
          src={openedBox === OPENED_BOX_SETTINGS ? iconSettingsGold : iconSettingsGrey}
          alt='settings'
          onClick={() => switchOpenedBox(OPENED_BOX_SETTINGS)}  
        />
        <img
          className={`${classes.footerOpt} ${classes.footerOptAbout}`}
          src={openedBox === OPENED_BOX_ABOUT ? iconAboutGold : iconAboutGrey}
          alt='about'
          onClick={() => switchOpenedBox(OPENED_BOX_ABOUT)}  
        />
      </div>
      <div
        className={`${classes.footerOpt} ${classes.footerOptLogout}`}
        onClick={() => logout(userDispatch, emit)}
      >
        <span className={classes.footerOptLogoutIcon} />
        <span>LOG OUT</span>
      </div>
    </div>
  );
}

export default SidebarFooter;
