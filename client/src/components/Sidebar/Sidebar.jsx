import { useState } from 'react';
import { createUseStyles } from 'react-jss';
import SidebarHeader from './SidebarHeader/SidebarHeader';
import SidebarFooter from './SidebarFooter/SidebarFooter';
import ChatsList from './ChatsList/ChatsList';
import { OPENED_BOX_SETTINGS } from '../../utils/openedBox';
import styles from './Sidebar.styles';

const useStyles = createUseStyles(styles);

function Sidebar({ switchOpenedBox, openedBox }) {
  const [ searchVal, setSearchVal ] = useState('');

  const clearSearch = () => setSearchVal('');

  const classes = useStyles(openedBox);

  return (
    <div className={classes.Sidebar}>
      <div className={classes.headerAndChatsList}>
        <SidebarHeader
          searchVal={searchVal}
          setSearchVal={setSearchVal}
          openSettings={() => switchOpenedBox(OPENED_BOX_SETTINGS)}
        />
        <ChatsList
          searchVal={searchVal}
          clearSearch={clearSearch}
        />
      </div>
      <SidebarFooter
        switchOpenedBox={switchOpenedBox}
        openedBox={openedBox}
      />
    </div>
  );
}

export default Sidebar;
