import commonStyles from '../../../utils/commonStyles';

const styles = {
  SidebarHeader: {
    width: '100%',
    height: commonStyles.sidebarHeaderHeight,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  userInfo: {
    width: '60%',
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    cursor: 'pointer'
  },
  avatar: {
    width: '40px',
    height: '40px',
    marginRight: '20px',
    borderRadius: '50%',
    display: 'inline-block',
    background: commonStyles.iconLoginProfileGold,
    backgroundSize: '100%'
  },
  searchBox: {
    width: '90%',
    position: 'relative',
    margin: '15px 0',
    padding: '10px',
    borderRadius: commonStyles.searchBoxBorderRadius,
    backgroundColor: commonStyles.searchBoxBgColor,
    boxShadow: commonStyles.boxShadowOutGreyMedium,
    '& form': {
      width: '100%',
      '& input': {
        width: '100%',
        textAlign: 'center',
        letterSpacing: '1px',
        backgroundColor: 'transparent',
        border: 'none',
        '&:focus': {
          outline: 'none'
        }
      }
    },
  },
  searchIcon: commonStyles.searchIcon
};

export default styles;
