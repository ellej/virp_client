import { useContext } from 'react';
import { createUseStyles } from 'react-jss';
import { UserContext } from '../../../contexts/user';
import styles from './SidebarHeader.styles';

const useStyles = createUseStyles(styles);

function SidebarHeader({ searchVal, setSearchVal, openSettings }) {
  const user = useContext(UserContext);

  const handleSearch = (e) => setSearchVal(e.target.value);

  const classes = useStyles();

  return (
    <div className={classes.SidebarHeader}>
      <div
        className={classes.userInfo}
        onClick={openSettings}
      >
        <span className={classes.avatar} />
        <span>{user.username}</span>
      </div>
      <div className={classes.searchBox}>
        <form onSubmit={(e) => e.preventDefault()}>
          <span className={classes.searchIcon} />
          <input
            type="text"
            id="search"
            autoComplete="off"
            spellCheck={false}
            placeholder="Search chats"
            value={searchVal}
            onChange={handleSearch}
          />
        </form>
      </div>
    </div>
  );
}

export default SidebarHeader;
