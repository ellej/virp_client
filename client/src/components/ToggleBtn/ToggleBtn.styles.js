import commonStyles from '../../utils/commonStyles';

const styles = {
  ToggleBtn: {
    width: ({ width }) => width ? width : '50px',
    height: ({ height }) => height ? height :  '50%',
    padding: ({ padding }) => padding ? padding :  '6px',
    display: 'flex',
    justifyContent: ({ isOn }) => isOn ? 'flex-start' : 'flex-end',
    border: `1px solid ${commonStyles.brandGoldMain}`,
    cursor: 'pointer'
  },
  activator: {
    width: '50%',
    height: '100%',
    backgroundColor: ({ isOn }) => isOn
      ? commonStyles.brandGoldMain
      : commonStyles.quietWhite,
    transition: 'all 150ms ease-out'
  }
};

export default styles;
