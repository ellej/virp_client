import { createUseStyles } from 'react-jss';
import styles from './ToggleBtn.styles';

const useStyles = createUseStyles(styles);

function ToggleBtn({ isOn, toggle, width, height, padding }) {
  const classes = useStyles({ isOn, width, height, padding });

  return (
    <div
      className={classes.ToggleBtn}
      onClick={() => toggle()}  
    >
      <div className={classes.activator} />
    </div>
  );
}

export default ToggleBtn;
