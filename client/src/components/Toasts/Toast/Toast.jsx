import { useContext } from 'react';
import { createUseStyles } from 'react-jss';
import { motion } from 'framer-motion';
import { ToastsDispatchContext } from '../../../contexts/toasts';
import { removeToast } from '../../../actions/toasts';
import {
  TOAST_INFO, TOAST_SUCCESS, TOAST_NEW_MESSAGE, TOAST_WARNING, TOAST_ERROR
} from '../../../utils/toastTypes';
import commonStyles from '../../../utils/commonStyles';
import styles from './Toast.styles';

const useStyles = createUseStyles(styles);

function Toast({ id, type, msg }) {
  const toastsDispatch = useContext(ToastsDispatchContext);

  const getIcon = () => {
    switch (type) {
      case TOAST_INFO:
        return <>&#8505;</>;
      case TOAST_SUCCESS:
        return <>&#10004;</>;
      case TOAST_NEW_MESSAGE:
        return <>&#9998;</>;
      case TOAST_WARNING:
        return <>&#9888;</>;
      case TOAST_ERROR:
        return <>&#9888;</>;
      default:
        return <>&#8505;</>;
    }
  };

  const classes = useStyles(type);    

  return (
    <motion.div
      initial={commonStyles.pageTransitionVar.initial}
      animate={commonStyles.pageTransitionVar.animate}
      exit={commonStyles.pageTransitionVar.exit}
      variants={commonStyles.pageTransition.popInOut}
      className={classes.Toast}
    >
      <div className={classes.icon}>{getIcon()}</div>
      <div className={classes.msg}>{msg}</div>
      <div
        className={classes.closeBtn}
        onClick={() => removeToast(id, toastsDispatch)}
      >
          &#10006;
      </div>
    </motion.div>
  );
}

export default Toast;
