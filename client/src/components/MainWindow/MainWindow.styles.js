const styles = {
  MainWindow: {
    width: '100%',
    height: '100vh',
    position: 'relative',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  }
};

export default styles;
