import { useState, useContext } from 'react';
import { motion } from 'framer-motion';
import { createUseStyles } from 'react-jss';
import { UserContext, UserDispatchContext } from '../../../contexts/user';
import { SocketEmitContext } from '../../../contexts/socket';
import { updateUser } from '../../../actions/user';
import SettingsHeader from './SettingsHeader/SettingsHeader';
import SettingsSidebar from './SettingsSidebar/SettingsSidebar';
import SettingsOptAccount from './SettingsOptAccount/SettingsOptAccount';
import SettingsOptNotifications from './SettingsOptNotifications/SettingsOptNotifications';
import SettingsOptPrivacy from './SettingsOptPrivacy/SettingsOptPrivacy';
import {
  OPENED_SETTINGS_ACCOUNT, OPENED_SETTINGS_NOTIFICATIONS, OPENED_SETTINGS_PRIVACY
} from '../../../utils/openedSettings';
import commonStyles from '../../../utils/commonStyles';
import styles from './SettingsBox.styles';

const useStyles = createUseStyles(styles);

function SettingsBox({ close }) {
  const [ openedSettings, setOpenedSettings ] = useState(OPENED_SETTINGS_ACCOUNT);
  const user = useContext(UserContext);
  const userDispatch = useContext(UserDispatchContext);
  const emit = useContext(SocketEmitContext);

  const getOpenedSettings = () => {
    switch (openedSettings) {
      case OPENED_SETTINGS_ACCOUNT:
        return (
          <SettingsOptAccount
            username={user.username}
            memberSince={user.createdAt}
            emit={emit}
          />
        );
      case OPENED_SETTINGS_NOTIFICATIONS:
        return (
          <SettingsOptNotifications
            newMsgNotificationOn={user.newMsgNotificationOn}
            userDispatch={userDispatch}
            updateUser={updateUser}
            emit={emit}
          />
        );
      case OPENED_SETTINGS_PRIVACY:
        return (
          <SettingsOptPrivacy
            defaultMsgDestruction={user.defaultMsgDestruction}
            decryptMsgByDefault={user.decryptMsgByDefault}
            userDispatch={userDispatch}
            updateUser={updateUser}
            emit={emit}
          />
        );
      default:
        return <></>;
    }
  };

  const switchOpenedSettings = (newVal) => {
    if (newVal !== openedSettings)
      setOpenedSettings(newVal);
  };

  const classes = useStyles();

  return (
    <motion.div
      initial={commonStyles.pageTransitionVar.initial}
      animate={commonStyles.pageTransitionVar.animate}
      exit={commonStyles.pageTransitionVar.exit}
      variants={commonStyles.pageTransition.horizontal}
      transition={commonStyles.customTransition.tween}
      className={classes.SettingsBox}
    >
      <SettingsHeader close={close} />
      <div className={classes.content}>
        <SettingsSidebar
          openedSettings={openedSettings}
          switchOpenedSettings={switchOpenedSettings}
        />
        {getOpenedSettings()}
      </div>
    </motion.div>
  );
}

export default SettingsBox;
