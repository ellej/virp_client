import commonStyles from '../../../../utils/commonStyles';

const styles = {
  accountSettingsItem: commonStyles.settingsOptItem,
  value: {
    color: commonStyles.noisyWhite,
    '& time': {
      fontSize: '12px',
      color: commonStyles.noisyWhite
    }
  },
  valueUsername: {
    cursor: 'not-allowed'
  },
  valuePassword: {
    cursor: 'not-allowed'
  },
  deleteBtn: commonStyles.deleteBtn
};

export default styles;
