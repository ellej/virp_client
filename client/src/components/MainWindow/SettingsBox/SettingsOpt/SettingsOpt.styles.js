import commonStyles from '../../../../utils/commonStyles';

const styles = {
  SettingsOpt: {
    width: `calc(100% - ${commonStyles.settingsSidebarWidth})`,
    marginLeft: '60px'
  },
  title: {
    color: commonStyles.brandGoldMain,
    fontSize: '14px',
    marginBottom: '30px'
  },
  info: {
    height: '100%',
    paddingRight: '10px',
    scrollbarWidth: 'thin', // firefox
    overflowY: 'auto',
    overflowX: 'hidden'
  }
};

export default styles;
