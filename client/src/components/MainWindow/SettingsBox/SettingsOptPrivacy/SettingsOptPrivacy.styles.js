import commonStyles from '../../../../utils/commonStyles';

const styles = {
  privacySettingsItem: commonStyles.settingsOptItem,
  itemSelfDestruction: {
    height: 'auto',
    marginTop: '40px',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
    '&:hover': {
      borderBottom: `1px solid ${commonStyles.brandGoldMain}`,
      paddingLeft: 0,
      '& span': {
        paddingLeft: '10px',
        color: commonStyles.brandGoldMain
      }
    },
    '& form': {
      height: '100%',
      width: '100%',
      margin: '20px 0',
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      '& label': {
        display: 'block',
        fontSize: '12px',
        color: commonStyles.noisyWhite,
        position: 'relative',
        paddingTop: '20px',
        cursor: 'pointer',
        userSelect: 'none',
        transition: 'all 120ms ease-in-out',
        '& input': {
          position: 'absolute',
          width: 0,
          height: 0,
          opacity: 0,
          cursor: 'pointer'
        },
        '&:hover': {
          color: commonStyles.brandGoldMain
        },
        '&:hover input ~ span': {
          backgroundColor: commonStyles.brandGoldMain
        },
        '& input:checked ~ span': {
          backgroundColor: commonStyles.brandGoldMain
        },
        '& input:checked ~ span:after': {
          display: 'block'
        },
        '& span': {
          position: 'absolute',
          top: 0,
          left: 0,
          width: '12px',
          height: '12px',
          marginLeft: '3px',
          backgroundColor: commonStyles.noisyWhite,
          borderRadius: '50%',
          '&:after': { // don't think this one is working
            content: '',
            position: 'absolute',
            display: 'none',
            top: '5px',
            left: '5px',
            width: '8px',
            height: '8px',
            borderRadius: '50%',
            background: 'white'
          }
        }
      }
    }
  },
  setting: {
    margin: '2px 0'
  }
};

export default styles;
