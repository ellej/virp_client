import { createUseStyles } from 'react-jss';
import styles from './SettingsHeader.styles';

const useStyles = createUseStyles(styles);

function SettingsHeader({ close }) {
  const classes = useStyles();

  return (
    <div className={classes.SettingsHeader}>
      <div className={classes.title}>
        settings
      </div>
      <div
        className={classes.closeBtn}
        onClick={close}  
      >
        &#8212;
      </div>
    </div>
  );
}

export default SettingsHeader;
