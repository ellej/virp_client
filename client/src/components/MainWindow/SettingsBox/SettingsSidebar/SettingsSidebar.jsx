import { createUseStyles } from 'react-jss';
import SettingsSidebarItem from './SettingsSidebarItem/SettingsSidebarItem';
import {
  OPENED_SETTINGS_ACCOUNT, OPENED_SETTINGS_NOTIFICATIONS, OPENED_SETTINGS_PRIVACY
} from '../../../../utils/openedSettings';
import styles from './SettingsSidebar.styles';

const useStyles = createUseStyles(styles);

function SettingsSidebar({ openedSettings, switchOpenedSettings }) {
  const classes = useStyles();

  return (
    <div className={classes.SettingsSidebar}>
      <SettingsSidebarItem
        name="account"
        isOpen={openedSettings === OPENED_SETTINGS_ACCOUNT}
        open={() => switchOpenedSettings(OPENED_SETTINGS_ACCOUNT)}
      />
      <SettingsSidebarItem
        name="notifications"
        isOpen={openedSettings === OPENED_SETTINGS_NOTIFICATIONS}
        open={() => switchOpenedSettings(OPENED_SETTINGS_NOTIFICATIONS)}
      />
      <SettingsSidebarItem
        name="privacy"
        isOpen={openedSettings === OPENED_SETTINGS_PRIVACY}
        open={() => switchOpenedSettings(OPENED_SETTINGS_PRIVACY)}
      />
    </div>
  );
}

export default SettingsSidebar;
