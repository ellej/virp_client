import commonStyles from '../../../../../utils/commonStyles';
import mediaQuerySizes from '../../../../../utils/mediaQuerySizes';

const styles = {
  SettingsSidebarItem: {
    width: '100%',
    height: '55px',
    fontSize: '14px',
    padding: '0 20px',
    display: 'flex',
    alignItems: 'center',
    color: ({ isOpen }) => isOpen ? commonStyles.brandGoldMain : commonStyles.noisyWhite,
    borderRight: ({ isOpen }) => isOpen ? `1px solid ${commonStyles.brandGoldMain}` : 'none',
    backgroundColor: ({ isOpen }) => isOpen ? commonStyles.quietBlack : 'transparent',
    cursor: 'pointer',
    transition: 'all 150ms ease-in-out',
    '&:hover': {
      backgroundColor: commonStyles.quietBlack
    },
    [mediaQuerySizes.maxWidth.lg()]: {
      padding: '0 10px',
      fontSize: '12px'
    },
    [mediaQuerySizes.maxHeightLaptop.lg()]: {
      fontSize: '12px'
    }
  }
};

export default styles;
