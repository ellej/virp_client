import commonStyles from '../../../../../utils/commonStyles';

const styles = {
  Contact: {
    width: '100%',
    height: '80px',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '10px 0',
    borderBottom: `1px solid ${commonStyles.quietWhite}`,
    transition: 'all 150ms ease-in-out',
    '& span': {
      transition: 'all 150ms ease-in-out'
    },
    '&:hover': {
      borderBottom: `1px solid ${commonStyles.brandGoldMain}`,
      paddingLeft: '10px',
      '& div span': {
        color: commonStyles.brandGoldMain
      }
    }
  },
  userInfo: {
    display: 'flex',
    alignItems: 'center',
    fontSize: '14px'
  },
  avatar: {
    width: '30px',
    height: '30px',
    marginRight: '20px',
    borderRadius: '50%',
    display: 'inline-block',
    background: commonStyles.iconLoginProfileGold,
    backgroundSize: '100%'
  },
  options: {
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  option: {
    marginLeft: '20px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    opacity: '0.7',
    cursor: 'pointer',
    transition: 'all 150ms ease-in-out'
  },
  optionStartChat: {
    cursor: ({ blocked, blockedByContact }) => blocked || blockedByContact
      ? 'not-allowed'
      : 'pointer',
    '&:hover': {
      opacity: ({ blocked, blockedByContact }) => blocked || blockedByContact
        ? '0.7'
        : '1'
    }
  },
  optionAdd: {
    cursor: ({ hasBeenAdded }) => hasBeenAdded ? 'not-allowed' : 'pointer',
    '&:hover': {
      opacity: ({ hasBeenAdded }) => hasBeenAdded ? '0.7' : '1'
    }
  },
  optionRemove: {
    cursor: ({ hasBeenAdded }) => hasBeenAdded ? 'pointer' : 'not-allowed',
    '&:hover': {
      opacity: ({ hasBeenAdded }) => hasBeenAdded ? '1' : '0.7'
    }
  },
  optionBlock: {
    height: '100%',
    '&:hover': {
      opacity: '1'
    },
  },
  optionIcon: {
    width: '27px',
    height: '27px'
  },
  optionDescription: {
    marginTop: '7px',
    fontSize: '10px',
    color: commonStyles.quietGray
  }
};

export default styles;
