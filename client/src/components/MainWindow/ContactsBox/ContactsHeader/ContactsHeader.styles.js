import commonStyles from '../../../../utils/commonStyles';

const styles = {
  ContactsHeader: {
    width: '100%',
    height: '250px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  heading: {
    ...commonStyles.mainWindowContentBoxHeader,
    marginBottom: '45px'
  },
  title: commonStyles.mainWindowContentBoxTitle,
  closeBtn: commonStyles.mainWindowContentBoxCloseBtn,
  searchBox: {
    width: '90%',
    position: 'relative',
    padding: '12px',
    borderRadius: commonStyles.searchBoxBorderRadius,
    boxShadow: commonStyles.boxShadowOutDarkMedium2,
    backgroundColor: commonStyles.searchBoxBgColor,
    '& input': {
      width: '100%',
      textAlign: 'center',
      letterSpacing: '1px',
      backgroundColor: 'transparent',
      border: 'none',
      '&:focus': {
        outline: 'none'
      }
    }
  },
  searchIcon: commonStyles.searchIcon
};

export default styles;
