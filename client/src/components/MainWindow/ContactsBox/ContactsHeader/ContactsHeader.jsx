import { useState } from 'react';
import { createUseStyles } from 'react-jss';
import { GET_CONTACT_SEARCH_RES } from '../../../../utils/socket';
import styles from './ContactsHeader.styles';

const useStyles = createUseStyles(styles);

function ContactsHeader({ emit, isSearching, setIsSearching, close }) {
  const [ searchVal, setSearchVal ] = useState('');

  const handleSearch = (e) => {
    const usernamePrefix = e.target.value;
    setSearchVal(usernamePrefix);

    if (isSearching && !usernamePrefix)
      setIsSearching(false);
    else if (!isSearching && usernamePrefix)
      setIsSearching(true);

    emit(GET_CONTACT_SEARCH_RES, usernamePrefix);
  };

  const classes = useStyles();

  return (
    <div className={classes.ContactsHeader}>
      <div className={classes.heading}>
        <div className={classes.title}>
          contacts
        </div>
        <div
          className={classes.closeBtn}
          onClick={close}  
        >
          &#8212;
        </div>
      </div>
      <div className={classes.searchBox}>
        <form onSubmit={(e) => e.preventDefault()}>
          <span className={classes.searchIcon} />
          <input
            type="text"
            id="search"
            spellCheck={false}
            placeholder="Search new or existing contacts"
            value={searchVal}
            onChange={handleSearch}
          />
        </form>
      </div>
    </div>
  );
}

export default ContactsHeader;
