import { useState, useContext } from 'react';
import { createUseStyles } from 'react-jss';
import Confirmation from '../../../Confirmation/Confirmation';
import { ChatsDispatchContext, OpenedChatDispatchContext } from '../../../../contexts/chats';
import { removeChat, setOpenedChat } from '../../../../actions/chats';
import { DELETE_CHAT } from '../../../../utils/socket';
import iconTrashRed from '../../../../assets/iconTrashRed.png';
import styles from './ChatHeader.styles';

const useStyles = createUseStyles(styles);

function ChatHeader({ header, chatId, emit, chatPartnerAccountDeleted }) {
  const chatsDispatch = useContext(ChatsDispatchContext);
  const openedChatDispatch = useContext(OpenedChatDispatchContext);
  const [ confirmationIsOpen, setConfirmationIsOpen ] = useState(false);

  const closeChat = () => setOpenedChat('', openedChatDispatch);
  const handleDeleteChat = () => {
    closeConfirmation();

    removeChat(chatId, chatsDispatch);
    closeChat();
    emit(DELETE_CHAT, chatId);
  };

  const openConfirmation = () => setConfirmationIsOpen(true);
  const closeConfirmation = () => setConfirmationIsOpen(false);

  const classes = useStyles({ chatPartnerAccountDeleted });

  return (
    <div className={classes.ChatHeader}>
      <div className={classes.name}>{header}</div>
      <div className={classes.options}>
        <span
          className={`${classes.closeBtn} ${classes.option}`}
          onClick={closeChat}
        >
          &#8212;
        </span>
        <img
          className={`${classes.deleteBtn} ${classes.option}`}
          src={iconTrashRed}
          alt='delete chat'
          onClick={openConfirmation}  
        />
      </div>
      {confirmationIsOpen && (
        <Confirmation
          title="Are you sure?"
          message="Please confirm that you wish to remove this chat."
          confirm={handleDeleteChat}
          cancel={closeConfirmation}
        />
      )}
    </div>
  );
}

export default ChatHeader;
