import commonStyles from '../../../../utils/commonStyles';

const styles = {
  ChatHeader: {
    ...commonStyles.mainWindowContentBoxHeader,
    height: '100px'
  },
  name: {
    ...commonStyles.mainWindowContentBoxTitle,
    color: ({ chatPartnerAccountDeleted }) => chatPartnerAccountDeleted
      ? commonStyles.quietGray
      : 'white'
  },
  options: {
    display: 'flex',
    alignItems: 'center',
  },
  option: {
    marginLeft: '25px'
  },
  closeBtn: commonStyles.mainWindowContentBoxCloseBtn,
  deleteBtn: commonStyles.deleteBtn
};

export default styles;
