const styles = {
  ChatMessages: {
    width: '100%',
    height: '100%',
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    padding: '20px 50px',
    overflowY: 'auto',
    overflowX: 'hidden'
  }
};

export default styles;
