import { useState, useEffect, useContext } from 'react';
import { createUseStyles } from 'react-jss';
import { motion } from 'framer-motion';
import { UserContext } from '../../../contexts/user';
import { SocketEmitContext } from '../../../contexts/socket';
import { getChatPartner } from '../../../services/user';
import ChatHeader from './ChatHeader/ChatHeader';
import ChatMessages from './ChatMessages/ChatMessages';
import ChatFooter from './ChatFooter/ChatFooter';
import { LOAD_CHAT_MESSAGES } from '../../../utils/socket';
import commonStyles from '../../../utils/commonStyles';
import styles from './ChatBox.styles';

const useStyles = createUseStyles(styles);

function ChatBox({ chat }) {
  const user = useContext(UserContext);
  const emit = useContext(SocketEmitContext);
  const [ chatPartner, setChatPartner ] = useState('');
  const [ chatPartnerAccountDeleted, setChatPartnerAccountDeleted ] = useState(false);
  const [ destructionOpt, setDestructionOpt ] = useState('');

  const changeDestructionOpt = (newOpt) => setDestructionOpt(newOpt);

  useEffect(() => {
    const setPartner = async () => {
      // the server returns an object with either an error or a data property
      const { data, error } = await getChatPartner(chat, user);
      if (data) {
        setChatPartnerAccountDeleted(false);
        setChatPartner(data);
      }
      // handle the case where the chat partner does not exist
      // (the chat partner might have deleted its account)
      else if (error) {
        setChatPartnerAccountDeleted(true);
        setChatPartner({ _id: '', username: 'account deleted' });
      }
    };

    setPartner();
  }, [chat._id]);

  useEffect(() => emit(LOAD_CHAT_MESSAGES, chat._id), [chat._id, emit]);

  useEffect(() => {
    setDestructionOpt(chat.members
        .find(member => member.user.toString() === user._id)
        .msgDestructionOpt
    );
  }, [chat._id]);

  const classes = useStyles();

  return (
    <>
    {chatPartner && (
      <motion.div
        initial={commonStyles.pageTransitionVar.initial}
        animate={commonStyles.pageTransitionVar.animate}
        exit={commonStyles.pageTransitionVar.exit}
        variants={commonStyles.pageTransition.horizontal}
        transition={commonStyles.customTransition.tween}
        className={classes.ChatBox}
      >
        <ChatHeader
          header={chatPartner.username}
          chatId={chat._id}
          emit={emit}
          chatPartnerAccountDeleted={chatPartnerAccountDeleted}
        />
        <ChatMessages
          username={user.username}
          messages={chat.messages}
          chatPartner={chatPartner}
          decryptMsgByDefault={user.decryptMsgByDefault}
          chatPartnerAccountDeleted={chatPartnerAccountDeleted}
        />
        <ChatFooter
          user={user}
          chat={chat}
          chatPartner={chatPartner}
          chatPartnerAccountDeleted={chatPartnerAccountDeleted}
          changeDestructionOpt={changeDestructionOpt}
          destructionOpt={destructionOpt}
        />
      </motion.div>
    )}
    </>
  );
}

export default ChatBox;
