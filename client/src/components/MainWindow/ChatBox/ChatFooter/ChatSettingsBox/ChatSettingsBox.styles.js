import commonStyles from '../../../../../utils/commonStyles';

const styles = {
  ChatSettingsBox: {
    width: '100%',
    height: '100%',
    margin: '20px 0',
    '& form': {
      height: '100%',
      width: '100%',
      paddingLeft: '40px',
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      '& label': {
        display: 'block',
        fontSize: '14px',
        color: commonStyles.quietGray,
        position: 'relative',
        paddingTop: '30px',
        cursor: 'pointer',
        userSelect: 'none',
        transition: 'all 120ms ease-in-out',
        '& input': {
          position: 'absolute',
          width: 0,
          height: 0,
          opacity: 0,
          cursor: 'pointer'
        },
        '&:hover': {
          color: commonStyles.brandGoldMain
        },
        '&:hover input ~ span': {
          backgroundColor: commonStyles.brandGoldMain
        },
        '& input:checked ~ span': {
          backgroundColor: commonStyles.brandGoldMain
        },
        '& input:checked ~ span:after': {
          display: 'block'
        },
        '& span': {
          position: 'absolute',
          top: 0,
          left: 0,
          width: '15px',
          height: '15px',
          marginLeft: '3px',
          backgroundColor: '#eee',
          borderRadius: '50%',
          '&:after': { // don't think this one is working
            content: '',
            position: 'absolute',
            display: 'none',
            top: '5px',
            left: '5px',
            width: '8px',
            height: '8px',
            borderRadius: '50%',
            background: 'white'
          }
        }
      },
      '& button': {
        width: '150px',
        height: '100%',
        padding: '0 15px',
        fontSize: '14px',
        color: commonStyles.quietGray,
        outline: 'none',
        border: 'none',
        borderLeft: `1px solid ${commonStyles.quietWhite}`,
        background: 'none',
        cursor: 'pointer',
        transition: 'all 150ms ease-in-out',
        '&:hover': {
          color: 'white'
        }
      }
    }
  }
};

export default styles;
