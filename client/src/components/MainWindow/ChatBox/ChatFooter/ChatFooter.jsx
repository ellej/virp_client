import { useState, useEffect } from 'react';
import { createUseStyles } from 'react-jss';
import TypingBox from './TypingBox/TypingBox';
import ChatSettingsBox from './ChatSettingsBox/ChatSettingsBox';
import iconBombWhite from '../../../../assets/iconBombWhite.png';
import iconEmojiWhite from '../../../../assets/iconEmojiWhite.png';
import styles from './ChatFooter.styles';

const useStyles = createUseStyles(styles);

function ChatFooter({ user, chat, chatPartner, chatPartnerAccountDeleted, changeDestructionOpt, destructionOpt }) {
  const [ settingsOpened, setSettingsOpened ] = useState(false);

  const openSettings = () => setSettingsOpened(true);
  const closeSettings = () => setSettingsOpened(false);

  useEffect(() => {
    if (settingsOpened)
      closeSettings();
  }, [chat]);

  const classes = useStyles();

  return (
    <div className={classes.ChatFooter}>
      <div className={classes.content}>
        {settingsOpened ? (
          <ChatSettingsBox
            chatId={chat._id}
            chatMembers={chat.members}
            userId={user._id}
            changeDestructionOpt={changeDestructionOpt}
            prevDestructionOpt={destructionOpt}
            close={closeSettings}
          />
        ) : (
          <>
          <div className={classes.options}>
            <img
              className={classes.option}
              src={iconBombWhite}
              alt='self destruction timer'
              onClick={openSettings}  
            />
            <img
              className={classes.option}
              src={iconEmojiWhite}
              alt='emoji'
              style={{ cursor: 'not-allowed' }} // update when implementing the feature
            />
          </div>
          <TypingBox
            chatId={chat._id}
            chatPartner={chatPartner}
            destructionOpt={destructionOpt}
            //defaultDestructionOpt={destructionOpt}
            usersPubKey={user.pubKey}
            chatPartnerAccountDeleted={chatPartnerAccountDeleted}
          />
          </>
        )}
      </div>
    </div>
  );
}

export default ChatFooter;
