import commonStyles from '../../../../../utils/commonStyles';

const textAreaWidth = '80%';
const sendBtnWidth = `calc(100% - ${textAreaWidth})`;

const styles = {
  TypingBox: {
    width: commonStyles.typingBoxWidth,
    height: '100%',
    '& form': {
      height: '100%',
      position: 'relative',
      display: 'flex',
      justifyContent: 'space-between',
      '& textarea': {
        height: '100%',
        width: textAreaWidth,
        padding: '30px',
        fontSize: '13px',
        letterSpacing: '1px',
        border: 'none',
        backgroundColor: 'transparent',
        cursor: ({ disabled }) => disabled ? 'not-allowed' : 'auto',
        resize: 'none',       // removes the resize handle
        overflow: 'hidden',   // removes scrollbar (but does not seem to work)
        transition: 'all 150ms ease-in-out',
        '&:focus': {
          padding: '15px',
          outline: 'none'
        }
      },
      '& button': {
        height: '100%',
        width: sendBtnWidth,
        fontSize: '60px',
        color: commonStyles.quietGray,
        outline: 'none',
        background: 'none',
        border: 'none',
        borderLeft: `1px solid ${commonStyles.quietWhite}`,
        cursor: ({ disabled }) => disabled ? 'not-allowed' : 'pointer',
        transition: 'all 150ms ease-in-out',
        '&:hover': {
          color: ({ disabled }) => disabled ? commonStyles.quietGray : 'white'
        }
      }
    }
  },
  loading: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    background: '#212637',
    zIndex: 1,
    '& p': {
      marginLeft: '20px',
      color: commonStyles.brandGoldMain,
      fontSize: '12px'
    }
  }
};

export default styles;
