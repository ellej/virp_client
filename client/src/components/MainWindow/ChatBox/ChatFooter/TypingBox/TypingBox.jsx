import { useState, useContext } from 'react';
import { createUseStyles } from 'react-jss';
import { SocketEmitContext } from '../../../../../contexts/socket';
import { ContactsContext } from '../../../../../contexts/contacts';
import useCryptography from '../../../../../hooks/useCryptography';
import useInput from '../../../../../hooks/useInput';
import Loading from '../../../../Loading/Loading';
import { SEND_MESSAGE } from '../../../../../utils/socket';
import commonStyles from '../../../../../utils/commonStyles';
import styles from './TypingBox.styles';

const useStyles = createUseStyles(styles);

function TypingBox({ chatId, chatPartner, destructionOpt, usersPubKey, chatPartnerAccountDeleted }) {
  const [ isSending, setIsSending ] = useState(false);
  const emit = useContext(SocketEmitContext);
  const contacts = useContext(ContactsContext);
  const { pubEncrypt } = useCryptography('');
  const [ text, handleTextChange, resetText ] = useInput('');

  const SPINNER_TIME = 1200;

  const disabled = () => {
    if (chatPartnerAccountDeleted)
      return true;

    const contact = contacts && contacts.filter(c => idsMatch(c._id, chatPartner._id))[0];
    
    return contact ? contact.blocked || contact.blockedByContact : false;
  };

  const getTextareaPlaceholder = () => {
    if (chatPartnerAccountDeleted)
      return 'Account deleted :(';

  return disabled() ? 'Blocked' : `Expires in ${destructionOpt}...`;
  };

  const idsMatch = (id1, id2) => id1.toString() === id2.toString();

  const handleSubmit = (e) => {
    e.preventDefault();

    if (!text || disabled())
      return;

    setIsSending(true);
    setTimeout(() => setIsSending(false), SPINNER_TIME);

    const msgData = {
      chatId,
      receiver: chatPartner,
      msgEncryptedForSender: pubEncrypt(text, usersPubKey),
      msgEncryptedForReceiver: pubEncrypt(text, chatPartner.pubKey),
      pubKey: '' // change to user's pubKey if needed later
    };

    emit(SEND_MESSAGE, msgData)
    resetText();
  };

  const listenForEnterKey = (e) => {
    const ENTER_KEY = 13;

    if (e.keyCode === ENTER_KEY && e.shiftKey)
      return;

    if (e.keyCode === ENTER_KEY)
      handleSubmit(e);
  };

  const classes = useStyles({ disabled: disabled() });

  return (
    <div className={classes.TypingBox}>
      <form onSubmit={handleSubmit}>
        {isSending && (
          <div className={classes.loading}>
            <Loading
              type="Bars"
              color={commonStyles.brandGoldMain}
              height={30}
              width={30}
              timeout={SPINNER_TIME}
            />
            <p>Encrypting...</p>
          </div>
        )}
        <textarea
          id="text"
          placeholder={getTextareaPlaceholder()}
          spellCheck={false}
          maxLength={200}
          value={text}
          onChange={handleTextChange}
          onKeyDown={listenForEnterKey}
          disabled={disabled()}
        />
        <button type="submit">
          &#10148;
        </button>
      </form>
    </div>
  );
}

export default TypingBox;
