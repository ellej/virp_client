import commonStyles from '../../../utils/commonStyles';

const styles = {
  ChatBox: {
    ...commonStyles.mainWindowContentBox,
    paddingBottom: 0,
    justifyContent: 'space-between'
  },
};

export default styles;
