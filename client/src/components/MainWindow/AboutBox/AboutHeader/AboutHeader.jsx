import { createUseStyles } from 'react-jss';
import styles from './AboutHeader.styles';

const useStyles = createUseStyles(styles);

function AboutHeader({ close }) {
  const classes = useStyles();

  return (
    <div className={classes.AboutHeader}>
      <div className={classes.title}>
        about
      </div>
      <div
        className={classes.closeBtn}
        onClick={close}  
      >
        &#8212;
      </div>
    </div>
  );
}

export default AboutHeader;
