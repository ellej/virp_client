import { useState } from 'react';
import { motion } from 'framer-motion';
import { createUseStyles } from 'react-jss';
import useInput from '../../hooks/useInput';
import commonStyles from '../../utils/commonStyles';
import styles from './Confirmation.styles';

const useStyles = createUseStyles(styles);

function Confirmation({ title, message, confirm, cancel, confirmText, cancelText, passwordRequired }) {
  const [ passwordFieldIsOpen, setPasswordFieldIsOpen ] = useState(false);
  const [ password, handlePasswordChange, resetPassword ] = useInput('');

  const handleConfirmation = () => {
    if (passwordRequired && !passwordFieldIsOpen)
      openPasswordField();
    else if (passwordFieldIsOpen)
      confirm(password);
    else
      confirm();
  };

  const openPasswordField = () => setPasswordFieldIsOpen(true);

  const classes = useStyles();

  return (
    <motion.div
      initial={commonStyles.pageTransitionVar.initial}
      animate={commonStyles.pageTransitionVar.animate}
      exit={commonStyles.pageTransitionVar.exit}
      variants={commonStyles.pageTransition.popInOut}
      className={classes.Confirmation}
    >
      <div className={classes.content}>
        <h2>{title}</h2>
        {passwordFieldIsOpen ? (
          <form onSubmit={(e) => e.preventDefault()}>
            <input
              type="password"
              autoComplete="off"
              id="password"
              placeholder="confirm password"
              value={password}
              onChange={handlePasswordChange}
              required
            />
          </form>
        ) : (
          <p>{message}</p>
        )}
        <div className={classes.buttons}>
          <button
            className={`${classes.button} ${classes.btnConfirm}`}
            onClick={handleConfirmation}
          >
            {confirmText || 'REMOVE'}
          </button>
          <button
            className={`${classes.button} ${classes.btnCancel}`}
            onClick={cancel}  
          >
            {cancelText || 'CANCEL'}
          </button>
        </div>
      </div>
    </motion.div>
  );
}

export default Confirmation;
