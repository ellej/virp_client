import commonStyles from '../../utils/commonStyles';

const styles = {
  Confirmation: {
    position: 'fixed',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 100
  },
  content: {
    width: '70%',
    padding: '30px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    border: '1px solid white',
    borderRadius: '140px',
    borderTopRightRadius: 0,
    boxShadow: commonStyles.boxShadowDownOutDarkLarge,
    backgroundColor: commonStyles.brandDarkMain,
    '& h2': {
      fontSize: '16px'
    },
    '& p': {
      fontSize: '14px'
    },
    '& h2, p': {
      textAlign: 'center',
      lineHeight: '2em',
      marginBottom: '30px'
    },
    '& form': {
      width: '90%',
      marginBottom: '30px',
      display: 'flex',
      justifyContent: 'center',
      '& input': {
        width: '85%',
        padding: '5px',
        fontSize: '14px',
        textAlign: 'center',
        backgroundColor: 'transparent',
        border: '1px solid rgba(255, 255, 255, 0.5)',
        transition: 'all 200ms ease-in',
        '&:focus': {
          width: '100%',
          outline: 'none'
        },
        '&:required': {
          boxShadow: 'none'
        }
      }
    }
  },
  buttons: {
    width: '50%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  button: {
    minWidth: '110px',
    margin: '0 10px',
    padding: '15px 0',
    fontWeight: 700,
    border: 'none',
    borderRadius: '30px',
    boxShadow: commonStyles.boxShadowOutDarkSmall,
    background: 'transparent',
    outline: 'none',
    cursor: 'pointer',
    transition: 'all 150ms ease-in-out',
    '&:hover': {
      boxShadow: commonStyles.boxShadowOutDarkMedium2,
      transform: 'translateY(-3px)'
    }
  },
  btnConfirm: {
    backgroundColor: commonStyles.red
  },
  btnCancel: {
    backgroundColor: commonStyles.quietWhite
  }
};

export default styles;
