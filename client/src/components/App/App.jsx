import { useEffect } from 'react';
import { UserProvider } from '../../contexts/user';
import { CryptoProvider } from '../../contexts/crypto';
import { ToastsProvider } from '../../contexts/toasts';
import MainSwitch from '../MainSwitch/MainSwitch';
import { wakeUpServer } from '../../services/wakeUpServer';

function App() {
  useEffect(() => {
    wakeUpServer();

    // Keep the server awake.
    const FOUR_MINUTES = 4 * 60_000;
    const intervalId = setInterval(wakeUpServer, FOUR_MINUTES);

    return () => clearInterval(intervalId);
  }, []);

  return (
    <UserProvider>
      <CryptoProvider>
        <ToastsProvider>
          <MainSwitch />
        </ToastsProvider>
      </CryptoProvider>
    </UserProvider>
  );
}

export default App;
