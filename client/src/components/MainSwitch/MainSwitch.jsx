import { useContext, useState } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { AnimatePresence } from 'framer-motion';
import { UserContext } from '../../contexts/user';
import { SocketProvider } from '../../contexts/socket';
import { ChatsProvider } from '../../contexts/chats';
import { ContactsProvider } from '../../contexts/contacts';
import { UnreadMsgsProvider } from '../../contexts/unreadMsgs';
import Homepage from '../../routes/Homepage/Homepage';
import AuthForm from '../../routes/AuthForm/AuthForm';
import Dashboard from '../../routes/Dashboard/Dashboard';
import About from '../../routes/About/About';
import Route404 from '../../routes/404/404';
import {
  ROUTE_HOME, ROUTE_SIGNUP, ROUTE_LOGIN,
  ROUTE_DASHBOARD, ROUTE_ABOUT } from '../../utils/routePaths';

function MainSwitch() {
  const { isAuthenticated } = useContext(UserContext);
  // Knowing whether a server authentication attempt has been made is used
  // for displaying a toast message on the first attempt, informing the user
  // that it may take a few seconds to log in (as the server may be waking up).
  const [hasAttemptedServerAuthentication, setHasAttemptedServerAuthentication] = useState(false);

  return (
    <AnimatePresence>
      <Switch>
        <Route exact path={ROUTE_HOME}>
          {isAuthenticated
            ? <Redirect to={ROUTE_DASHBOARD} />
            : <Homepage />
          }
        </Route>
        <Route exact path={ROUTE_SIGNUP}>
          {isAuthenticated
            ? <Redirect to={ROUTE_DASHBOARD} />
            : <AuthForm
                isNewUser={true}
                hasAttemptedServerAuthentication={hasAttemptedServerAuthentication}
                setHasAttemptedServerAuthentication={setHasAttemptedServerAuthentication}
              />
          }
        </Route>
        <Route exact path={ROUTE_LOGIN}>
          {isAuthenticated
            ? <Redirect to={ROUTE_DASHBOARD} />
            : <AuthForm
                isNewUser={false}
                hasAttemptedServerAuthentication={hasAttemptedServerAuthentication}
                setHasAttemptedServerAuthentication={setHasAttemptedServerAuthentication}
              />
          }
        </Route>
        <Route exact path={ROUTE_DASHBOARD}>
          {isAuthenticated
            ? <ChatsProvider>
                <ContactsProvider>
                  <UnreadMsgsProvider>
                    <SocketProvider>
                      <Dashboard />
                    </SocketProvider>
                  </UnreadMsgsProvider>
                </ContactsProvider>
              </ChatsProvider>
            : <Redirect to={ROUTE_HOME} />
          }
        </Route>
        <Route exact path={ROUTE_ABOUT}>
          <About />
        </Route>
        <Route>
          <Route404 />
        </Route>
      </Switch>
    </AnimatePresence>
  );
}

export default MainSwitch;
