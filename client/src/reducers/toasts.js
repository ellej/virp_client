import { ADD_TOAST, REMOVE_TOAST } from '../actions/actionTypes';

const reducer = (state, action) => {
  switch (action.type) {
    case ADD_TOAST:
      const toast = {
        id: action.toastId,
        type: action.toastType,
        msg: action.toastMsg
      };
      return [ ...state, toast ];
    case REMOVE_TOAST:
      return state.filter(t => t.id !== action.toastId);
    default:
      throw new Error(`Unhandled type: ${action.type}`);
  }
};

export default reducer;
