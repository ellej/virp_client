import {
  ADD_CONTACTS, ADD_CONTACT, UPDATE_CONTACT, ADD_SEARCH_RES, REMOVE_CONTACT
} from '../actions/actionTypes';

export const contactsReducer = (state, action) => {
  switch (action.type) {
    case ADD_CONTACTS:
      return action.contacts;
    case ADD_CONTACT:
      return contactAlreadyExists(action.contact, state)
        ? state
        : [ ...state, action.contact ];
    case UPDATE_CONTACT:
      if (!state)
        return state;

      return state.map(contact => idsMatch(contact._id, action.contactId)
        ? { ...contact, ...action.updates }
        : contact
      );
    case REMOVE_CONTACT:
      return state.filter(contact => !idsMatch(contact._id, action.id));
    default:
      throw new Error(`Unhandled type: ${action.type}`);
  }
};

export const searchResReducer = (state, action) => {
  switch (action.type) {
    case ADD_SEARCH_RES:
      return action.res;
    default:
      throw new Error(`Unhandled type: ${action.type}`);
  }
};

const contactAlreadyExists = (contact, list) => list && list.some(
  c => c.username.toLowerCase() === contact.username.toLowerCase()
);

const idsMatch = (id1, id2) => id1.toString() === id2.toString();
