# Virp | Encrypted Messaging Platform

Virp is a privacy-focused web-based messaging platform allowing users to chat via end-to-end encrypted (E2EE) messages. Users can control self-destruction timers for outgoing messages, manage notifications, settings, active chats, as well as their contacts list by searching, adding, removing, or blocking other users.

## Table of Contents

- [Tech Stack](#tech-stack-frontend-and-backend)
- [Live Application](#live-application)
- [Screenshot](#screenshot)
- [Demo](#demo)
- [Planning and Project Details](#planning-and-project-details)
- [Getting Started (Server)](#getting-started-server)
  - [Server prerequisites](#server-prerequisites)
  - [Install server dependencies](#install-server-dependencies)
  - [Configure the port to listen to](#configure-the-port-to-listen-to)
  - [Configure the dev client URL](#configure-the-dev-client-url)
  - [Configure the name of the database](#configure-the-name-of-the-database)
  - [Run the server application](#run-the-server-application)
- [Getting Started (Client)](#getting-started-client)
  - [Client prerequisites](#client-prerequisites)
  - [Install client dependencies](#install-client-dependencies)
  - [Configure your dev server URL](#configure-your-dev-server-url)
  - [Run the client application](#run-the-client-application)
- [Running the Automated Tests](#running-the-automated-tests)

## Tech Stack

* Node.js
* Express
* MongoDB
* React.js
* Socket.io
* Jest

## Live Application

The [client](./client) application is currently running at:

* https://virp.vercel.app

## Screenshot

![Virp screenshot](https://res.cloudinary.com/ellej/image/upload/c_scale,f_auto,q_auto,w_1920/v1615294657/ellej.dev/project-virp_e3omzv.png)

## Demo

A demo video of how the application works can be found at:

* https://youtu.be/xMQcHOBvlgM

## Planning and Project Details

For planning and project details (e.g. requirements, security implementations, UML diagrams) see:

* https://www.ellej.dev/projects/virp

## Getting Started (Server)

The following instructions explain how to run the [server](./server/) application in **dev mode** locally.

### Server prerequisites

This server-side application requires that you have:

* [Node.js](https://nodejs.org/)
* [MongoDB Compass (local)](https://www.mongodb.com/download-center/compass) or [MongoDB Atlas (cloud)](https://www.mongodb.com/atlas) (see below)

##### Set up MongoDB Compass (local database with GUI)

Part A. Download MongoDB Compass
1. Go to the [download page](https://www.mongodb.com/download-center/compass).
2. Select the latest version of *Stable*.
3. Select the platform you are using.
4. Click *Download*.

Part B. Install MongoDB Compass and get it started
1. Go to the [installation guide](https://docs.mongodb.com/manual/administration/install-community/).
2. Select the guide appropriate for your operating system and follow the steps.

##### OR: Set up MongoDB Atlas (cloud)

1. Register or log in to [MongoDB Atlas](https://www.mongodb.com/atlas) and set up a free cluster.

### Install server dependencies

```bash
cd server
npm install
```

### Configure the port to listen to

Decide which port (e.g. `3001`) you want the server to listen for incoming requests from.

In [`config.js`](./server/config.js), set the value of the port number as the exported `SERVER_PORT_DEV` constant.

```javascript
exports.SERVER_PORT_DEV = 3001;
```

From the example above, the server will be listening at `http://localhost:3001`, which is the URL that will also be used when [configuring the client application](#getting-started-client).

### Configure the dev client URL

The server will be listening for web socket events from the client, so it needs to know the client URL. If you do not make any modifications to the port or URL that the client application will run on, then the client URL for development will be `http://localhost:5173` and you do not need to configure anything.

To modify the dev client URL, in [`config.js`](./server/config.js) set the URL as the exported `CLIENT_URL_DEV` constant.

Example:
```javascript
exports.CLIENT_URL_DEV = 'http://localhost:5173';
```

### Configure the name of the database

In the `server` directory, create a file called `.env`. Copy the contents of [`sample.env`](./server/sample.env) into your new `.env` file.

**If "I installed MongoDB locally":**

If you installed MongoDB locally as described in the [prerequisites](#server-prerequisites) section, the application is now configured to write to a database called `chat_app`.

If you already have a database in MongoDB called `chat_app` or want to use one with a different name for this application, go to the file `.env` and replace the latter part of `MONGODB_URI` as well as `MONGODB_URI_TEST` with your preferred name.

Example:

```shell
MONGODB_URI=mongodb://localhost/<YOUR NEW DB NAME>
MONGODB_URI_TEST=mongodb://localhost/<YOUR NEW DB NAME>_test
```

**If "I'm using MongoDB Atlas":**

If you want to modify the MongoDB connection string (e.g. if you are using MongoDB Atlas), replace the values of `MONGODB_URI` and `MONGODB_URI_TEST` in `.env` with the correct connection string.

Example:

```shell
MONGODB_URI=mongodb+srv://<YOUR DB USER>:<YOUR DB USER PASSWORD>@<MONGODB URL>/chat_app?retryWrites=true&w=majority
MONGODB_URI_TEST=mongodb+srv://<YOUR DB USER>:<YOUR DB USER PASSWORD>@<MONGODB URL>/chat_app_test?retryWrites=true&w=majority
```

> The database does **not** need to be created in advance. MongoDB automatically creates it if it does not exist.

### Run the server application

From the `server` directory, run:

```bash
npm start
```


## Getting Started (Client)

The following instructions explain how to run the [client](./client/) application in **dev mode** locally.

### Client prerequisites

The client-side application requires that you have:

* [Node.js](https://nodejs.org/)
* The [backend server](#getting-started-server) application set up and running

### Install client dependencies

```bash
cd client
npm install
```

### Configure your dev server URL

If you have set up your backend server you can copy the [port number](#configure-the-port-to-listen-to) (e.g. `3001`) that the server is listening on.

Once copied, open [`/src/config/urls.js`](./client/src/config/urls.js) and paste it as part of the `SERVER_URL_DEV` constant.

Example:
```javascript
const SERVER_URL_DEV = 'http://localhost:3001';
```

Additionally, in `package.json` add the same URL to the `proxy` field.

```json
{
  "proxy": "http://localhost:3001"
}
```

### Run the client application

Make sure your [backend server is running](#run-the-server-application) in a separate terminal.

From the `client` directory, run:

```bash
npm start
```

Open a browser and navigate to [http://localhost:5173](http://localhost:5173).


## Running the Automated Tests

If you have completed the steps in the [Getting Started (Server)](#getting-started-server) section, navigate to the `server` directory and run the tests:

```bash
cd server
npm run test
```
